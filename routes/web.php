<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'HomeController@index')->name('home');
//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/', array('as' => 'home', 'uses' => 'UserController@index'));
//Route::get('/', ['uses'=>'HomeController@index','as'=>'home']); 

Route::get('/', array('uses' => 'HomeController@index'));
Route::get('home', array('uses' => 'HomeController@index'));

Route::get('test/{id}', array('as' => 'test', 'uses' => 'HomeController@show'));
Route::get('news/{id}', array('as' => 'test', 'uses' => 'HomeController@show'));

/*************
 * ADMIN
 *************/
Route::get('admin', array('as' => 'admin', 'uses' => 'AdminController@index'));
Route::get('/admin/signin', array('as' => 'login', 'uses' => 'AdminController@showLogin'));
Route::post('/admin/signin', array('uses' => 'AdminController@authen'));
Route::get('/admin/signup', array('uses' => 'AdminController@showRegister'));
Route::post('admin/signup', array('uses' => 'Auth\RegisterController@register'));
Route::get('/admin/logout', 'UserController@Authlogout');
Route::get('/admin/profile', array('uses' => 'AdminController@index'));
Route::get('/admin/news', array('uses' => 'NewsController@index'));
Route::get('/admin/news/create', array('uses' => 'NewsController@create'));
Route::post('/admin/news/create', array('uses' => 'NewsController@store'));
Route::get('/admin/news/edit/{id}', array('uses' => 'NewsController@edit'));
Route::post('/admin/news/edit/{id}', array('uses' => 'NewsController@update'));
Route::get('/admin/news/delete/{id}', array('uses' => 'NewsController@destroy'));
Route::get('/upload/news/{filename}', function ($filename)
{
	return Image::make(storage_path('app/news/uploads/' . $filename))->response();
});

Route::get('storage/{filename}', function ($filename)
{
    return Image::make(storage_path('app/news/uploads/' . $filename))->response();
});

//category
Route::get('/admin/category', array('as' => 'admin', 'uses' => 'CategoryController@index'));