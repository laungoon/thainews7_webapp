<!DOCTYPE html>
<html lang="en">

<head>

@include('includes.admin.head')

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        @include('includes.admin.sidebar')
        
        <!-- Page Content -->
        <div id="page-wrapper">
            @yield('content')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('admintheme/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('admintheme/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('admintheme/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('admintheme/dist/js/sb-admin-2.js') }}"></script>

    <!-- DataTables JavaScript -->
    <script src="{{ asset('admintheme/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admintheme/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admintheme/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>
    
    @yield('javascript')
</body>

</html>
