<?php

?>

@extends('layouts.authen')
@section('content')

<div class="content">
    <div id="login" class="p-8">

        <div class="form-wrapper md-elevation-8 p-8">

            <div class="logo bg-primary">
                <span>F</span>
            </div>

            <div class="title mt-4 mb-8">Log in to your account</div>

            <form name="loginForm" novalidate action="signin" method="post">
            	<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group mb-4">
                    <input type="email" class="form-control" id="loginFormInputEmail" name="email" 
                           aria-describedby="emailHelp"
                           placeholder=" "/>
                    <label for="loginFormInputEmail">Email address</label>
                </div>

                <div class="form-group mb-4">
                    <input type="password" class="form-control" id="loginFormInputPassword" name="password" 
                           placeholder="Password"/>
                    <label for="loginFormInputPassword">Password</label>
                </div>

                <div class="remember-forgot-password row no-gutters align-items-center justify-content-between pt-4">

                    <div class="form-check remember-me mb-4">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" aria-label="Remember Me"/>
                            <span class="checkbox-icon"></span>
                            <span class="form-check-description">Remember Me</span>
                        </label>
                    </div>

                    <a href="#" class="forgot-password text-primary mb-4">Forgot Password?</a>
                </div>

                <button type="submit" class="submit-button btn btn-block btn-primary my-4 mx-auto"
                        aria-label="LOG IN">
                    LOG IN
                </button>

            </form>

            <div class="separator">
                <span class="text">OR</span>
            </div>



            <div
                class="register d-flex flex-column flex-sm-row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                <span class="text mr-sm-2">Don't have an account?</span>
                <a class="link text-primary" href="register">Create an account</a>
            </div>

        </div>
    </div>
</div>	

@stop
