@extends('layouts.default')
@section('content')

<div class="theme-padding">

    <!-- Navigation Holder -->
    <div class="container">

        <!-- Main Content Row -->
        <div class="row">

            <!-- small sidebar -->
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 r-hidden">

                <!-- add widget -->
                <div class="aside-add">
                    <a href="#"><img src="{{ asset('theme/images/aside-add.jpg') }}" alt=""></a>
                </div>
                <!-- add widget -->

                <aside class="aside">


                    <!-- video posts -->
                    <div class="widget">
                        <h3>video posts</h3>

                        <div class="video-posts">
                            <ul>
                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="{{ asset('theme/images/post-1/img-01.jpg') }}" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>                                               
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="{{ asset('theme/images/post-1/img-02.jpg') }}" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="{{ asset('theme/images/post-1/img-03.jpg') }}" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="{{ asset('theme/images/post-1/img-04.jpg') }}" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="{{ asset('theme/images/post-1/img-05.jpg') }}" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="{{ asset('theme/images/post-1/img-06.jpg') }}" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- video posts -->
                </aside>
            </div>
            <!-- small sidebar -->

            <!-- Content -->
            <div class="col-lg-7 col-md-7 col-sm-9 col-xs-8 r-full-width">


                <!-- list posts -->
                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ข่าวใหม่ THAINEWS 7</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <!--NewsCate2 Start-->
                            
                            @foreach($data['updateNews'] as $key => $valueUpdateNews)
                            <li class="row no-gutters">

                                <!-- thumbnail -->

                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                  
                                     <img src="{{ asset('/upload/news/'. $valueUpdateNews->image) }}" alt="">

                                        <span class="post-badge">THAINEWS7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">{{ $valueUpdateNews->title }}</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2017</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>
                                        <?php
                                        $string = $valueUpdateNews->title;
                                        echo str_limit($string, 250);
                                        ?>
                                         <a href="/news/{{ $valueUpdateNews->id }}" class="read-more"> อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>
                            @endforeach
                            <!--NewsCate2 End-->

                        </ul>
                    </div>
                    <!-- List Post -->
                </div>
                <!--  list posts -->

                <!-- บันเทิง thainews7 -->

                <!-- list posts -->
                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>เจาะข่าวเด่น ประเด็นเด็ด</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                     @foreach($data['newsCat1'] as $key1 => $value1)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="{{ asset('/upload/news/'. $value1->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">{{$value1->title}}</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2017</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>
                                        <?php 
                                        $string = $value1->content;
                                        echo str_limit($string, 250);
                                        ?>
                                        
                                        <a href="/news/{{ $value1->id }}" class="read-more"> อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->

                </div>

                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>บันเทิง Thainews7</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    @foreach($data['newsCat2'] as $key2 => $value2)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                    <img src="{{ asset('/upload/news/'. $value2->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="#">{{ $value2->title }}</a></h4>
                                         <a href="/news/{{ $value2->id }}" class="read-more"> อ่านต่อ...</a>
                                          <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7 </li>
                                            <li><i class="fa fa-clock-o"></i>{{ $value2->create_date}}</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>
                                        <?php
                                       $string = $value2->content;
                                        echo str_limit($string, 200); 
                                       ?>
                                          <a href="/news/{{ $value2->id }}" class="read-more"> อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->
                              

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->

                </div>
                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ศิลปะ วัฒธรรม ความเชื่อ</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    @foreach($data['newsCat3'] as $key3 => $value3)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="{{ asset('/upload/news/'. $value3->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">{{$value3->title}}</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7</li>
                                            <li><i class="fa fa-clock-o"></i>{{ $value3->create_date}}</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                          <?php
                                       $string = $value3->content;

                                        echo str_limit($string, 150); 
                                       ?>
                                        <p> <a href="/news/{{ $value3->id }}" class="read-more">อ่านต่อ ..</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->

                </div>

                <div class="post-widget">
                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ท่องเที่ยว แฟชั่น ความงาม</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    @foreach($data['newsCat4'] as $key4 => $value4)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="{{ asset('/upload/news/'. $value4->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">{{ $value4->title }}</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7 </li>
                                            <li><i class="fa fa-clock-o"></i>{{ $value4->create_date}}</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>

                                        <p> <?php
                                       $string = $value4->content;

                                       echo str_limit($string, 150); // กำหนดความยาวของตัวอักษรหน้าแรก"
                                       ?>

                                        <a href="/news/{{ $value4->id }}"" class="read-more"> อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->
                </div>

                <div class="post-widget">
                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>เจาะหลังไมค์ ไทยลูกทุ่ง</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    @foreach($data['newsCat5'] as $key5 => $value5)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="{{ asset('/upload/news/'. $value5->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                <h4><a href="listing-page-2.html">{{ $value5->title}}</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7</li>
                                            <li><i class="fa fa-clock-o"></i>{{ $value5->create_date}}</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>
                                        <?php
                                        $string = $value5->content;
                                        echo str_limit($string, 250);
                                        ?>

                                        <a href="/news/{{ $value5->id }}"" class="read-more">อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->
                </div>

                <div class="post-widget">
                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ข่าวประชาสัมพันธ์</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    @foreach($data['newsCat6'] as $key6 => $value6)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="{{ asset('/upload/news/'. $value6->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">{{ $value6->title }}</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7</li>
                                            <li><i class="fa fa-clock-o"></i>{{ $value6->create_date}}</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>
                                        <?php
                                        $string = $value6->content;
                                        echo str_limit($string, 250);
                                        ?>
                                         <a href="/news/{{ $value6->id }}"" class="read-more">อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->
                </div>

                <div class="post-widget">
                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ก้าวทันโลก</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    @foreach($data['newsCat7'] as $key7 => $value7)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="{{ asset('/upload/news/'. $value7->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="/news/{{ $value7->id }}></a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i> Thainews7</li>
                                            <li><i class="fa fa-clock-o"></i>{{ $value7->create_date}}</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>
                                       <?php
                                       $string = $value7->content;
                                       echo str_limit($string, 250);
                                       ?>

                                       <a href="/news/{{ $value7->id }}" class="read-more">อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->
                </div>

                <div class="post-widget">
                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ข่าวเด่นภูมิภาค 77 จังหวัด</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    @foreach($data['newsCat8'] as $key8 => $value8)
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="{{ asset('/upload/news/'. $value8->image) }}" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">{{ $value8->title }}</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>Thainews7 </li>
                                            <li><i class="fa fa-clock-o"></i>{{ $value8->create_date}}</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p> 
                                        <a href="/news/{{ $value8->id }}"" class="read-more">อ่านต่อ...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>

                        </ul>
                    </div>
                    @endforeach
                    <!-- List Post -->
                </div>
                
                <!-- Gallery Widget -->
                <div class="post-widget">

                    <!-- Heading -->
                    <div class="primary-heading">
                        <h2>gallery style</h2>
                    </div>
                    <!-- Heading -->

                   <div class="gallery-widget">
            <!-- gallery slides -->
            <ul class="gallery-slider" id="gallery-slider">
                   @foreach($data['newsCat9'] as $key9 => $value9)
                <li>
                    <div class="post-thumb">
                        <img src="{{ asset('/upload/news/'. $value9->image) }}" alt="">
                        
                    </div>
                </li>

            @endforeach
            </ul>


        </div>

                </div>
                <!-- Gallery Widget -->

                <!-- add banner -->
                <div class="add-banner text-center m-0 p-0">
                    <img src="{{ asset('theme/images/add-3.jpg') }}" alt="">
                </div>
                <!-- add banner -->

            </div>
            <!-- Content -->

            <!-- Sidebar -->
            <div class="col-lg-3 col-md-3 col-xs-12 custom-re">
                <div class="row">
                    <aside class="side-bar grid">
                        <!-- Auther profile -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">

                            <div class="widget light-shadow">
                                <h3 class="secondry-heading">our authers</h3>
                                <div class="auther-widget">
                                    <div id="auther-slider-thumb>
                                         <a data-slide-index="0" href="#"><img src="{{ asset('/upload/news/AD190x500.jpg') }}" alt=""></a>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Auther profile -->

                        <!-- Social Networks -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <ul class="aside-social">
                                    <li class="fb">
                                        <a href="#"><i class="fa fa-facebook"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li class="pi">
                                        <a href="#"><i class="fa fa-pinterest-p"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li class="tw">
                                        <a href="#"><i class="fa fa-twitter"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li class="gmail">
                                        <a href="#"><i class="fa fa-google-plus"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li class="sky">
                                        <a href="#"><i class="fa fa-skype"></i><span>followers</span><em>100</em></a>
                                    </li>
                                    <li class="yt">
                                        <a href="#"><i class="fa fa-youtube"></i><span>followers</span><em>100</em></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Social Networks -->

                        <!-- twitter widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">twitter feed</h3>
                                <ul class="twitter-feed" id="twitter-slider">

                                    <!-- tweet -->
                                    <li>
                                        <div class="twitter-brand-name">
                                            <h6><i class="fa fa-twitter"></i>
                                                <a href="#">@Envato Studio</a>
                                                <i class="fa fa-retweet pull-right"></i>
                                            </h6>
                                        </div>
                                        <div class="brand-name">
                                            <p>Could @bodletech screen tery life to new have lengths ?</p>
                                            <span class="site-link"><a href="#">http://www.telegraph.co.uk/</a>6 hour ago</span>
                                        </div>
                                    </li> 
                                    <!-- tweet -->

                                    <!-- tweet -->
                                    <li>
                                        <div class="twitter-brand-name">
                                            <h6><i class="fa fa-twitter"></i>
                                                <a href="#">@Envato Studio</a>
                                                <i class="fa fa-retweet pull-right"></i>
                                            </h6>
                                        </div>
                                        <div class="brand-name">
                                            <p>Could @bodletech screen tery life to new have lengths ?</p>
                                            <span class="site-link"><a href="#">http://www.telegraph.co.uk/</a>6 hour ago</span>
                                        </div>
                                    </li> 
                                    <!-- tweet -->

                                </ul>
                            </div>
                        </div>
                        <!-- twitter widget -->

                        <!-- News Widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">top news</h3>
                                <div class="horizontal-tabs-widget">

                                    <!-- tabs navs -->
                                    <ul class="theme-tab-navs">
                                        <li class="active">
                                            <a href="#week" data-toggle="tab">week</a>
                                        </li>
                                        <li>
                                            <a href="#month" data-toggle="tab">month</a>
                                        </li>
                                        <li>
                                            <a href="#all" data-toggle="tab">all</a>
                                        </li>
                                    </ul>
                                    <!-- tabs navs -->

                                    <!-- Tab panes -->
                                    <div class="horizontal-tab-content tab-content">
                                        <div class="tab-pane fade active in" id="week">
                                            <ul class="post-wrap-list">
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-01.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-02.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-03.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane fade" id="month">
                                            <ul class="post-wrap-list">
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-01.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-02.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolor...</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-03.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane fade" id="all">
                                            <ul class="post-wrap-list">
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-01.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-02.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="post-wrap small-post">
                                                    <div class="post-thumb">
                                                        <img src="{{ asset('theme/images/sidebar/news/img-03.jpg') }}" alt="post">
                                                    </div>
                                                    <div class="post-content">
                                                        <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                        <ul class="post-meta">
                                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                            <li><i class="fa fa-comments-o"></i>20</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Tab panes -->

                                </div>
                            </div>
                        </div>
                        <!-- News Widget -->

                        <!-- Calender widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">Arcieve</h3>
                                <div class="calender-widget">
                                    <div id="calendar" class="calendar"></div>
                                </div>
                            </div>
                        </div>
                        <!-- Calender widget -->

                        <!-- Slider Widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">slider widget</h3>
                                <div class="slider-widget">

                                    <!-- Slider -->
                                    <div id="post-slider">
                                        <div class="item">
                                            <img src="{{ asset('theme/images/sidebar/slider-01.jpg') }}" alt="slider">
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('theme/images/sidebar/slider-01.jpg') }}" alt="slider">
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('theme/images/sidebar/slider-01.jpg') }}" alt="slider">
                                        </div>
                                    </div>
                                    <!-- Slider -->

                                    <!-- Content -->
                                    <div class="post-content">
                                        <h4><a href="#">Girl yoyo Music listing</a></h4>

                                        <!-- post meta -->
                                        <ul class="post-meta">
                                            <li><i class="icon-user"></i>jessica alex</li>
                                            <li><i class="icon-clock"></i>5 Min ago</li>
                                        </ul>
                                        <!-- post meta -->

                                        <p>Lorem ipsum dolor siting amet consec adipisicing elit, sed do eiusmod.</p>
                                    </div>
                                    <!-- Content -->

                                </div>
                            </div>
                        </div>
                        <!-- Slider Widget -->


                        <!-- facebook widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">find us on facebook</h3>
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=300&height=220&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" width="300" height="220" style="border:none;overflow:hidden"></iframe>
                            </div>
                        </div>
                        <!-- facebook widget -->

                        <!-- Catogires widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">categories</h3>
                                <ul class="categories-widget">
                                    <li><a href="#"><em>fashion</em><span class="bg-green">15</span></a></li>
                                    <li><a href="#"><em>world</em><span class="bg-masterd">15</span></a></li>
                                    <li><a href="#"><em>technology</em><span class="bg-p-green">15</span></a></li>
                                    <li><a href="#"><em>health</em><span class="bg-orange">15</span></a></li>
                                    <li><a href="#"><em>lifestyle</em><span class="bg-gray">15</span></a></li>
                                    <li><a href="#"><em>sports</em><span class="bg-masterd">15</span></a></li>

                                </ul>
                            </div>
                        </div>
                        <!-- Catogires widget -->

                        <!-- sports Widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">sports news</h3>
                                <div class="horizontal-tabs-widget">

                                    <!-- tabs navs -->
                                    <ul class="theme-tab-navs style-2">
                                        <li class="active">
                                            <a href="#fixtures" data-toggle="tab">Last Match</a>
                                        </li>
                                        <li>
                                            <a href="#result" data-toggle="tab">upcomming</a>
                                        </li>
                                    </ul>
                                    <!-- tabs navs -->

                                    <!-- Tab panes -->
                                    <div class="horizontal-tab-content tab-content">
                                        <div class="tab-pane fade active in" id="fixtures">
                                            <div class="matches-detail">
                                                <p>49 Chapel Lane ARNE BH20 12/02/2016 / 19:00</p>
                                                <div class="team-btw-match">
                                                    <ul>
                                                        <li>
                                                            <img src="{{ asset('theme/images/team-logos/img-03.png') }}" alt="">
                                                            <span>Munchester <br> united</span>
                                                        </li>
                                                        <li>
                                                            <img src="{{ asset('theme/images/team-logos/img-04.png') }}" alt="">
                                                            <span>Norwich <br> City</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="goals-detail">
                                                    <ul>
                                                        <li>
                                                            <span>M. Johansen</span>
                                                            <span>23 (FNA)</span>
                                                        </li>
                                                        <li>
                                                            <span>G. Smith</span>
                                                            <span>41 (FNA)</span>
                                                        </li>
                                                        <li>
                                                            <span>T. Mosler</span>
                                                            <span>59 (FNA)</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="result">
                                            <div class="upcoming-fixture">
                                                <div class="table-responsive">
                                                    <table class="table m-0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="logo-width-name"><img src="{{ asset('theme/images/team-logos-small/img-01.png') }}" alt="">South</div>
                                                                </td>
                                                                <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="logo-width-name"><img src="{{ asset('theme/images/team-logos-small/img-03.png') }}" alt="">South</div>
                                                                </td>
                                                                <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="logo-width-name"><img src="{{ asset('theme/images/team-logos-small/img-05.png') }}" alt="">South</div>
                                                                </td>
                                                                <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="logo-width-name"><img src="{{ asset('theme/images/team-logos-small/img-07.png') }}" alt="">South</div>
                                                                </td>
                                                                <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Tab panes -->

                                </div>
                            </div>
                        </div>
                        <!-- sports Widget -->

                        <!-- advertisement widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">advertisement</h3>
                                <div class="add">
                                    <a href="#"><img src="{{ asset('theme/images/add.jpg') }}" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <!-- advertisement widget -->

                        <!-- advertisement widget -->
                        <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                            <div class="widget">
                                <h3 class="secondry-heading">online pool</h3>
                                <div class="pool-widget">
                                    <p>Type and scrambeld it to make a type specimen book:</p>
                                    <form role="form">
                                        <div class="radio">
                                            <label><input type="radio" name="optradio">Twice a year</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="optradio">Once a year</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="optradio">Only when i need it</label>
                                        </div>
                                        <div class="radio">
                                            <label><input type="radio" name="optradio">I'm healthy</label>
                                        </div>
                                    </form>
                                    <div class="group">
                                        <a href="#" class="btn red">vote now</a>
                                        <a href="#" class="btn red">view now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- advertisement widget -->
                    </aside>
                </div>
            </div>
            <!-- Sidebar -->

        </div>   
        <!-- Main Content Row -->

    </div>
</div>  

<!-- categories posts -->
<div class="post-widget m-0 white-bg">
    <div class="cate-tab-navs">
        <div class="container">
            <ul class="nav-justified">
                <li class="active"><a data-target="#politics" data-toggle="tab">politics</a></li>
                <li><a data-target="#technology" data-toggle="tab">technology</a></li>
                <li><a data-target="#fashion" data-toggle="tab">fashion</a></li>
                <li><a data-target="#international" data-toggle="tab">international</a></li>
                <li><a data-target="#admin" data-toggle="tab">admin pick</a></li>
                <li><a data-target="#top" data-toggle="tab">top posts</a></li>
            </ul>
        </div>
    </div>
    <div class="cate-tab-content theme-padding">
        <div class="container">
            <div class="tab-content">
                <div class="tab-pane active fade in" id="politics">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/world/img-01.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/world/img-02.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/world/img-03.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/world/img-05.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="technology">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/technology/img-05.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/technology/img-02.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/technology/img-03.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/technology/img-04.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="fashion">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/fashion/img-05.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/fashion/img-04.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/fashion/img-03.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/fashion/img-08.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Confirm Information</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="international">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/world/img-08.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/world/img-07.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/fashion/img-06.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/world/img-05.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="admin">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/sports/img-08.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/sports/img-07.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/technology/img-08.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/lifestyle/img-08.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="top">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/technology/img-04.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/lifestyle/img-05.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/technology/img-02.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="{{ asset('theme/images/health/img-03.jpg') }}" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>     
<!-- categories posts -->

@stop