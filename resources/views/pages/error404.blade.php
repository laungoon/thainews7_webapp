<!DOCTYPE html>
<html lang="en">

    <head>
     
   
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>EVENT</title>

        <!-- STYLESHEETS -->
        <style type="text/css">
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
        </style>

        <!-- Icons.css -->
        <link type="text/css" rel="stylesheet" href="{{ asset('theme/assets/icons/fuse-icon-font/style.css') }}">

        <!-- Animate.css -->
        <link type="text/css" rel="stylesheet" href="{{ asset('theme/assets/vendor/animate.css/animate.min.css') }}">

        <!-- PNotify -->
        <link type="text/css" rel="stylesheet" href="{{ asset('theme/assets/vendor/pnotify/pnotify.custom.min.css') }}">

        <!-- Nvd3 - D3 Charts -->
        <link type="text/css" rel="stylesheet" href="{{ asset('theme/assets/vendor/nvd3/build/nv.d3.min.css') }}"/>

        <!-- Perfect Scrollbar -->
        <link type="text/css" rel="stylesheet" href="{{ asset('theme/assets/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>

        <!-- Fuse Html -->
        <link type="text/css" rel="stylesheet" href="{{ asset('theme/assets/vendor/fuse-html/fuse-html.min.css') }}"/>

        <!-- Main CSS -->
        <link type="text/css" rel="stylesheet" href="{{ asset('theme/assets/css/main.css') }}">
        <!-- / STYLESHEETS -->

        <!-- JAVASCRIPT -->

        <!-- jQuery -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/jquery/dist/jquery.min.js') }}"></script>

        <!-- Mobile Detect -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/mobile-detect/mobile-detect.min.js') }}"></script>

        <!-- Perfect Scrollbar -->
        <script type="text/javascript"
                src="{{ asset('theme/assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>

        <!-- Popper.js -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/popper.js/index.js') }}"></script>

        <!-- Bootstrap -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/bootstrap/bootstrap.min.js') }}"></script>

        <!-- Nvd3 - D3 Charts -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/d3/d3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/nvd3/build/nv.d3.min.js') }}"></script>

        <!-- Data tables -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>

        <script type="text/javascript"
                src="{{ asset('theme/assets/vendor/datatables-responsive/js/dataTables.responsive.js') }}"></script>

        <!-- PNotify -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/pnotify/pnotify.custom.min.js') }}"></script>

        <!-- Fuse Html -->
        <script type="text/javascript" src="{{ asset('theme/assets/vendor/fuse-html/fuse-html.min.js') }}"></script>

        <!-- Main JS -->
        <script type="text/javascript" src="{{ asset('theme/assets/js/main.js') }}"></script>

        <!-- / JAVASCRIPT -->

    <body class="layout layout-vertical layout-left-navigation layout-below-toolbar">

        <div id="wrapper">

            <aside id="aside" class="aside aside-left"
                   data-fuse-bar="aside" data-fuse-bar-media-step="md"
                   data-fuse-bar-position="left">
               
            </aside>

            <div class="content-wrapper">

               
                <div class="content">
                    <div id="error-404" class="d-flex flex-column align-items-center justify-content-center">

                        <div class="content">

                            <div class="error-code display-1 text-center">404</div>

                            <div class="message h4 text-center text-muted">Sorry but we couldn’t find the page you are
                                looking for
                            </div>

                            <div
                                class="search md-elevation-1 row no-gutters align-items-center mt-12 mb-4 bg-white text-auto">
                                <i class="col-auto icon-magnify s-6 mx-4"></i>
                                <input class="col" type="text" placeholder="Search for anything">
                            </div>

                            <a class="back-link d-block text-center text-primary" href="/">Go back to dashboard</a>

                        </div>
                    </div>
                </div>

            </div>

            
        </div>

    </body>
</html>