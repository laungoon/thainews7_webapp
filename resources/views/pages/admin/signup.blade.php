<?php

?>

@extends('layouts.admin.authen')
@section('content')


<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Create an accoun</h3>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="panel-body">
                <form name="loginForm" role="form" action="/admin/signup" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Name" name="name" type="name" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="E-mail" name="email" type="email">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="password" type="password" value="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Confirm Password" name="password_confirmation" type="password" value="">
                        </div>
                        
                        <div class="form-group">
                            <div class="form-check mr-1 mb-1">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" aria-label="Remember Me"/>
                                    <span class="checkbox-icon"></span>
                                    <span>I read and accept</span>
                                </label>
                            </div>
                            <a href="#" class="text-primary mb-1">terms and conditions</a>
                        </div>

                        <button type="submit" class="submit-button btn btn-block btn-primary my-4 mx-auto"
                                aria-label="LOG IN">
                            CREATE MY ACCOUNT
                        </button>


                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- 
<div class="content">
    <div id="register" class="p-8">

        <div class="form-wrapper md-elevation-8 p-8">

            <div class="logo bg-primary">
                <span>F</span>
            </div>

            <div class="title mt-4 mb-8">Create an account</div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form name="registerForm" action="auth/signup" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group mb-4">
                    <input type="text" class="form-control" id="registerFormInputName" name="name" 
                           aria-describedby="nameHelp"/>
                    <label for="registerFormInputName">Name</label>
                </div>

                <div class="form-group mb-4">
                    <input type="email" class="form-control" id="registerFormInputEmail" name="email" 
                           aria-describedby="emailHelp"/>
                    <label for="registerFormInputEmail">Email address</label>
                </div>

                <div class="form-group mb-4">
                    <input type="password" class="form-control" id="registerFormInputPassword" name="password" />
                    <label for="registerFormInputPassword">Password</label>
                </div>

                <div class="form-group mb-4">
                    <input type="password" class="form-control" id="registerFormInputPasswordConfirm" name="password_confirmation" />
                    <label for="registerFormInputPasswordConfirm">Password (Confirm)</label>
                </div>

                <div class="terms-conditions row align-items-center justify-content-center pt-4 mb-8">
                    <div class="form-check mr-1 mb-1">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" aria-label="Remember Me"/>
                            <span class="checkbox-icon"></span>
                            <span>I read and accept</span>
                        </label>
                    </div>
                    <a href="#" class="text-primary mb-1">terms and conditions</a>
                </div>

                <button type="submit" class="submit-button btn btn-block btn-primary my-4 mx-auto"
                        aria-label="LOG IN">
                    CREATE MY ACCOUNT
                </button>

            </form>

            <div
                class="login d-flex flex-column flex-sm-row align-items-center justify-content-center mt-8 mb-6 mx-auto">
                <span class="text mr-sm-2">Already have an account?</span>
                <a class="link text-primary" href="pages-auth-login.html">Log in</a>
            </div>

        </div>
    </div>
</div> -->	

@stop
