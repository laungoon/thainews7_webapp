<?php
    $newsId = Request::segment(4);
?>

@extends('layouts.admin.default')
@section('content')

 <!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Edite Category
            </div>

	        <!-- if there are creation errors, they will show here -->
			{{ HTML::ul($errors->all()) }}

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form" action="/admin/cate/update/{{$cate->id}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" placeholder="Enter name" name="category_name" value="{{ $cate->category_name }}">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="Active" {{ $cate->status == 'Active' ? 'selected="selected"' : '' }}>Active</option>
                                    <option value="NoActive" {{ $cate->status == 'NoActive' ? 'selected="selected"' : '' }}>Unactive</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->



@stop


@section('javascript')

  <script type="text/javascript">
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
  </script>

@stop