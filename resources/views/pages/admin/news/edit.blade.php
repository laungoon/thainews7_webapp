<?php
    $newsId = Request::segment(4);
?>

@extends('layouts.admin.default')
@section('content')

 <!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Edite News
            </div>

	        <!-- if there are creation errors, they will show here -->
			{{ HTML::ul($errors->all()) }}

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form" action="/admin/news/edit/{{ $newsId }}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label>Category</label>
                                <label>Category</label>
                                <select class="form-control" name="category_id">
                                @if($cate->count() > 0)
                                @foreach($cate as $cate) 
                                    <option value="{{$cate->id}}" {{ $news->category_id == $cate->id ? 'selected="selected"' : '' }}>{{$cate->category_name}}</option>
                                @endForeach
                                @else
                                No Record Found
                                    @endif   
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" placeholder="Enter title" name="title" value="{{ $news->title }}">
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <textarea class="form-control" rows="3" name="content">{{ $news->content }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>File input</label>
                                <input type="file" name="image" id="image">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control">
                                    <option value="Active" {{ $news->status == 'Active' ? 'selected="selected"' : '' }}>Active</option>
                                    <option value="NoActive" {{ $news->status == 'NoActive' ? 'selected="selected"' : '' }}>Unactive</option>
                                </select>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->



@stop


@section('javascript')

  <script type="text/javascript">
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
  </script>

@stop