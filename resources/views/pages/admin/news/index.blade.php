
@extends('layouts.admin.default')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">News</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               <button type="button" class="btn btn-success btn-circle btn-xl" id="createNews" ><i class="fa fa-plus"></i></button>
            </div>

             <!-- will be used to show any messages -->
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Category</th>
                            <th>Title</th>
                            
                            
                            <th>Image</th>
                            <th>Content</th>

                            <th>Status</th>
                            <th>Acction</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($news as $key => $value)
                        <tr class="odd gradeX">
                            <td>{{ $value->category_id }}</td>
                            <td>{{ $value->title }}</td>                            
                            

                            <td><img class="product-image" src="/upload/news/{{ $value->image }}" width="250"></td>
                            <td>{{ $value->content }}</td>


                            <td class="center">{{ $value->status }}</td>
                            <td class="center">
                                <button type="button" class="btn btn-success" onclick="editNews('{{ $value->id }}')">Edit</button>
                                <button type="button" class="btn btn-danger" onclick="deleteNews('{{ $value->id }}')">Delete</button>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                <!-- /.table-responsive -->
                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

@stop


@section('javascript')

  <script type="text/javascript">
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });

    $('#createNews').click(function() {
        window.location.href = "/admin/news/create";
    });

    function editNews(newsId)
    {
        window.location.href = "/admin/news/edit/" + newsId;
    }

    function deleteNews(newsId)
    {
        window.location.href = "/admin/news/delete/" + newsId;
    }
  </script>

@stop