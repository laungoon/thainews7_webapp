extends('layouts.default')
@section('content')

<div class="theme-padding">
                   
    <!-- Navigation Holder -->
    <header class="header">
        <div class="container">
            <div class="nav-holder">

                <!-- Logo Holder -->
                <div class="logo-holder">
                    <a href="index.html" class="inner-logo"></a>
                </div>
                <!-- Logo Holder -->

                <!-- Navigation -->
                <div class="cr-navigation">

                    <!-- Navbar -->
                     <nav class="cr-nav">
                        <ul>
                            <li>
                                <a href="index.html">หน้าแรก</a>
                                <ul>
                                   <li><a href="index-2.html">Home 2</a></li> 
                                   <li><a href="index-3.html">Home 3</a></li> 
                                </ul>
                            </li>
                   
                 
                            <li>
                                <a href="#">ข่าวบันเทิง</a>
                                <ul>
                                   <li><a href="products.html">ข่าวบันเทิง</a></li> 
                                   <li><a href="products-detail.html">แฟชั่น</a></li> 
                                </ul>
                            </li>
                            <li>
                                <a href="#">contact</a>
                                <ul>
                                   <li><a href="contact.html">ข่าวกีฬา</a></li> 
                                   <li><a href="contact-2.html">contact 2</a></li> 
                                   <li><a href="contact-3.html">contact 3</a></li> 
                                </ul>
                            </li>
                           <li>
                                <a href="#">ข่าวบันเทิง</a>
                            </li>
                            <li>
                                <a href="#">ข่าวกีฬา</a>
                            </li>
                            <li>
                                <a href="#">pages</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- Navbar -->

                    <ul class="cr-add-nav">
                        <li><a href="#" class="icon-search md-trigger" data-modal="search-popup"></a></li>
                        <li><a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a></li>
                    </ul>

                </div>
                <!-- Navigation -->

            </div>
        </div>
    </header>
    <!-- Navigation Holder -->
    <div class="container">

        <!-- Main Content Row -->
        <div class="row">

            <!-- small sidebar -->
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-4 r-hidden">

                <!-- add widget -->
                <div class="aside-add">
                    <a href="#"><img src="images/aside-add.jpg" alt=""></a>
                </div>
                <!-- add widget -->

                <aside class="aside">


                    <!-- video posts -->
                    <div class="widget">
                        <h3>video posts</h3>

                        <div class="video-posts">
                            <ul>
                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="images/post-1/img-01.jpg" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>                                               
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="images/post-1/img-02.jpg" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="images/post-1/img-03.jpg" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="images/post-1/img-04.jpg" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="images/post-1/img-05.jpg" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="post style-1">
                                        <div class="post-thumb"> 
                                            <img src="images/post-1/img-06.jpg" alt="">
                                            <a href="https://www.youtube.com/watch?v=eZKMrds77nE" data-rel="prettyPhoto"><i class="fa fa-play"></i></a>  
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="post-detail.html">Fight for voter is start Democrat this end</a></h4>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- video posts -->
                </aside>
            </div>
            <!-- small sidebar -->

            <!-- Content -->
            <div class="col-lg-7 col-md-7 col-sm-9 col-xs-8 r-full-width">

                <!-- Detail Post Widget -->
                <div class="post-widget">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                            <!-- Secondary Heading -->
                         <!--    <div class="primary-heading">
                                <h2>เทคโนโลยี</h2>
                            </div> -->
                            <!-- Secondary Heading -->

                            <!-- Post -->
                            <div class="post style-2 white-bg light-shadow">

                                <!-- Post Img -->
                                <div class="post-thumb">
                                    <img src="https://www.beartai.com/wp-content/uploads/2017/09/iphone-8-1000x600.png" alt="detail">
                                    <span class="post-badge">เทคโนโลยี</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Post Img -->

                                <!-- Post Detil -->
                                <div class="post-content cat-listing">
                                    <h4><a href="listing-page-3.html">นักวิเคราะห์ฟันธง iPhone X เจอปัญหาของขาดตลาดแน่นอนแถมสี...</a></h4>
                                    <p>ปัญหาที่ทั้งหญิงชายหลายๆ คนเป็น แล้วก็เวลาเพื่อนทักทีไรก็เสียเซลฟ์ทุกที...</p>
                                    <ul class="post-wrap-list">

                                        <!-- small thumb post -->
                                        <li class="post-wrap big-post">
                                            <div class="post-content">
                                                <ul class="post-meta">
                                                    <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i class="fa fa-comments-o"></i>20</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <!-- small thumb post -->

                                    </ul>
                                </div>
                                <!-- Post Detail -->

                            </div>
                            <!-- Post -->

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                    

                            <!-- Post -->
                            <div class="post style-2 white-bg light-shadow">

                                <!-- Post Img -->
                                <div class="post-thumb">
                                    <img src="http://s.isanook.com/he/0/ud/0/2113/istock_000052859468_medium.jpg" alt="detail">
                                    <span class="post-badge">สุขภาพ</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Post Img -->

                                <!-- Post Detil -->
                                <div class="post-content cat-listing">
                                    <h4><a href="listing-page-3.html">ไขข้อสงสัย! ขอบตาดำคล้ำ เพราะนอนไม่พอจริงหรือเปล่า</a></h4>
                                    <p>ปัญหาที่ทั้งหญิงชายหลายๆ คนเป็น แล้วก็เวลาเพื่อนทักทีไรก็เสียเซลฟ์ทุกที ก็คือเรื่องของ ขอบตาดำ</p>
                                    <ul class="post-wrap-list">

                                        <!-- small thumb post -->
                                        <li class="post-wrap big-post">
                                            <div class="post-content">
                                                <ul class="post-meta">
                                                    <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                    <li><i class="fa fa-comments-o"></i>20</li>
                                                </ul>
                                            </div>
                                        </li>
                                        <!-- small thumb post -->

                                    </ul>
                                </div>
                                <!-- Post Detail -->

                            </div>
                            <!-- Post -->
                                
                        </div>
                    </div>                            
                </div>
                <!-- Detail Post Widget -->

                <!-- list posts -->
                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ข่าวใหม่</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/8361/cockles.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">จริงหรือไม่? หอยแครง อันตราย! เสี่ยงมะเร็ง-เนื้องอก</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>หากใครเล่น social network แล้วเพื่อนมากๆ อาจจะเคยได้รับ forward message ในกลุ่มเฟซบุ๊ค หรือไลน์ส่งต่อๆ กันมา โดยมีข้อความเตือนว่า หมอที่ศิริราชฝากเตือนทุกคน ห้ามทานหอยแครง... <a href="#" class="read-more">read more...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/he/0/ud/1/8361/cockles-2.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">หอยแครง อันตราย?</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                                                       <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/7929/fit.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                        </ul>
                    </div>
                    <!-- List Post -->

                </div>
                <!--  list posts -->

                <!-- Detail list post -->
                <div class="post-widget">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <!-- main heading -->
                            <div class="primary-heading">
                                <h2> บันเทิง Thainews7</h2>
                            </div>
                            <!-- main heading -->

                            <!-- list of features -->
                            <div class="post style-2 featured-single-post white-bg light-shadow">

                                <div class="post-thumb">
                                    <img src="https://s.isanook.com/au/0/rp/r/w700h420/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2F1LzAvdWQvMTIvNjAyNTcvMTAxLmpwZw==.jpg" alt="detail">
                                    <span class="post-badge">บันเทิง </span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>

                                </div>

                                <div class="post-content">
                                    <h4><a href="listing-page-6.html">Audi A5 Coupe 2017 ใหม่ วางจำหน่ายอย่างเป็นทางการในประเทศไทย มีให้เลือกทั้งหมด 4 รุ่นย่อย พร้อมระบบขับเคลื่อนสี่ล้อ Quattro เคาะราคาจำหน่ายเริ่มต้น 3,299,000 บาท</a></h4>

                                     <p>หากใครเล่น social network แล้วเพื่อนมากๆ อาจจะเคยได้รับ forward message ในกลุ่มเฟซบุ๊ค หรือไลน์ส่งต่อๆ กันมา โดยมีข้อความเตือนว่า หมอที่ศิริราชฝากเตือนทุกคน ห้ามทานหอยแครง... <a href="#" class="read-more">read more...</a></p>


                                </div>

                            </div>
                            <!-- list of features -->

                        </div>
                    </div>
                </div>

                <div class="post-widget">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <!-- main heading -->
                            <div class="primary-heading">
                                <h2> ศิลปะ วัฒธรรม ความเชื่อ</h2>
                            </div>
                            <!-- main heading -->

                            <!-- list of features -->
                            <div class="post style-2 featured-single-post white-bg light-shadow">

                                <div class="post-thumb">
                                    <img src="https://s.isanook.com/au/0/rp/r/w700h420/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2F1LzAvdWQvMTIvNjAyNTcvMTAxLmpwZw==.jpg" alt="detail">
                                    <span class="post-badge">Thainews7</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>

                                </div>

                                <div class="post-content">
                                    <h4><a href="listing-page-6.html">Audi A5 Coupe 2017 ใหม่ วางจำหน่ายอย่างเป็นทางการในประเทศไทย มีให้เลือกทั้งหมด 4 รุ่นย่อย พร้อมระบบขับเคลื่อนสี่ล้อ Quattro เคาะราคาจำหน่ายเริ่มต้น 3,299,000 บาท</a></h4>

                                     <p>หากใครเล่น social network แล้วเพื่อนมากๆ อาจจะเคยได้รับ forward message ในกลุ่มเฟซบุ๊ค หรือไลน์ส่งต่อๆ กันมา โดยมีข้อความเตือนว่า หมอที่ศิริราชฝากเตือนทุกคน ห้ามทานหอยแครง... <a href="#" class="read-more">read more...</a></p>

                                </div>

                            </div>
                            <!-- list of features -->

                        </div>
                    </div>
                </div>
                <!-- Detail list post -->

                <!-- add Banner -->
                <div class="add-banner text-center post-widget p-0">
                    <img src="images/add-3.jpg" alt="">
                </div>
                <!-- add Banner -->

                <!-- list posts -->
                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ท่องเที่ยว แฟชั่น ความงาม</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/8361/cockles.jpg" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">จริงหรือไม่? หอยแครง อันตราย! เสี่ยงมะเร็ง-เนื้องอก</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>หากใครเล่น social network แล้วเพื่อนมากๆ อาจจะเคยได้รับ forward message ในกลุ่มเฟซบุ๊ค หรือไลน์ส่งต่อๆ กันมา โดยมีข้อความเตือนว่า หมอที่ศิริราชฝากเตือนทุกคน ห้ามทานหอยแครง... <a href="#" class="read-more">read more...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/he/0/ud/1/8361/cockles-2.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">หอยแครง อันตราย?</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/7929/fit.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/au/0/ud/11/59901/140.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/au/0/ud/11/59901/104.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                        </ul>
                    </div>
                    <!-- List Post -->

                </div>
                                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ท่องเที่ยว แฟชั่น ความงาม</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/8361/cockles.jpg" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">จริงหรือไม่? หอยแครง อันตราย! เสี่ยงมะเร็ง-เนื้องอก</a></h4>
                                      
                                        <p>หากใครเล่น social network แล้วเพื่อนมากๆ อาจจะเคยได้รับ forward message ในกลุ่มเฟซบุ๊ค หรือไลน์ส่งต่อๆ กันมา โดยมีข้อความเตือนว่า หมอที่ศิริราชฝากเตือนทุกคน ห้ามทานหอยแครง... <a href="#" class="read-more">read more...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/he/0/ud/1/8361/cockles-2.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">หอยแครง อันตราย?</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/7929/fit.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/au/0/ud/11/59901/140.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/au/0/ud/11/59901/104.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                        </ul>
                    </div>
                    <!-- List Post -->

                </div>
                                <div class="post-widget">

                    <!-- Main Heading -->
                    <div class="primary-heading">
                        <h2>ท่องเที่ยว แฟชั่น ความงาม</h2>
                    </div>
                    <!-- Main Heading -->

                    <!-- List Post -->
                    <div class="p-30 light-shadow white-bg">
                        <ul class="list-posts">
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/8361/cockles.jpg" alt="">
                                        <span class="post-badge">Thainews7</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">
                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">จริงหรือไม่? หอยแครง อันตราย! เสี่ยงมะเร็ง-เนื้องอก</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>หากใครเล่น social network แล้วเพื่อนมากๆ อาจจะเคยได้รับ forward message ในกลุ่มเฟซบุ๊ค หรือไลน์ส่งต่อๆ กันมา โดยมีข้อความเตือนว่า หมอที่ศิริราชฝากเตือนทุกคน ห้ามทานหอยแครง... <a href="#" class="read-more">read more...</a></p>
                                    </div>
                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/he/0/ud/1/8361/cockles-2.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">หอยแครง อันตราย?</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="http://s.isanook.com/he/0/ud/1/7929/fit.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/au/0/ud/11/59901/140.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                            <li class="row no-gutters">

                                <!-- thumbnail -->
                                <div class="col-sm-4 col-xs-12">
                                    <div class="post-thumb"> 
                                        <img src="https://s.isanook.com/au/0/ud/11/59901/104.jpg" alt="">
                                        <span class="post-badge">sport</span>
                                        <div class="thumb-hover">
                                            <div class="position-center-center">
                                                <a href="#" class="fa fa-link"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- thumbnail -->

                                <!-- post detail -->
                                <div class="col-sm-8 col-xs-12">

                                    <div class="post-content">
                                        <h4><a href="listing-page-2.html">4 กุญแจสำคัญสู่การ “ลดน้ำหนัก” ให้ได้ผล</a></h4>
                                        <ul class="post-meta">
                                            <li><i class="fa fa-user"></i>jessica alex</li>
                                            <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                            <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                            <li><i class="fa fa-comments-o"></i>20</li>
                                        </ul>
                                        <p>อย่างไรก็ตาม ถึงแม้ว่าหอยแครงจะไม่ได้ก่อให้เกิดโรคมะเร็งอย่างที่เข้าใจกัน แต่อย่างไรเสียหอยแครงก็ยังเป็นสาเหตุที่ก่อให้เกิดความผิดปกติ <a href="#" class="read-more">read more...</a></p>
                                    </div>

                                </div>
                                <!-- post detail -->

                            </li>
                        </ul>
                    </div>
                    <!-- List Post -->

                </div>
                <!--  list posts -->

                <!-- Gallery Widget -->
                <div class="post-widget">

                    <!-- Heading -->
                    <div class="primary-heading">
                        <h2>gallery style</h2>
                    </div>
                    <!-- Heading -->

                    <div class="gallery-widget">

                        <!-- gallery slides -->
                        <ul class="gallery-slider" id="gallery-slider"> 
                            <li>
                                <div class="post-thumb">
                                    <img src="https://s.isanook.com/au/0/ud/11/59901/140.jpg" alt="">
                                    <div class="thumb-over">
                                        <span class="tag"><i class="fa fa-camera"></i>16 photos</span>
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</a></h4>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="post-thumb">
                                    <img src="https://s.isanook.com/au/0/ud/12/60257/109.jpg" alt="">
                                    <div class="thumb-over">
                                        <span class="tag"><i class="fa fa-camera"></i>16 photos</span>
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</a></h4>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="post-thumb">
                                    <img src="https://s.isanook.com/au/0/ud/12/60257/102.jpg" alt="">
                                    <div class="thumb-over">
                                        <span class="tag"><i class="fa fa-camera"></i>16 photos</span>
                                        <h4><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua.</a></h4>
                                    </div>
                                </div>
                            </li>



                        </ul>
                        <!-- gallery slides -->

                        <!-- gallery thumbs -->
                        <ul id="gallery-slider-thumb" class="gallery-slider-thumb">
                            <li data-slideIndex="0"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-01.jpg" alt="">
                                </div>                                
                            </a></li>
                            <li data-slideIndex="1"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-02.jpg" alt="">
                                </div>                                
                            </a></li>
                            <li data-slideIndex="2"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-03.jpg" alt="">
                                </div>                                
                            </a></li>
                            <li data-slideIndex="3"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-04.jpg" alt="">
                                </div>                                
                            </a></li>
                            <li data-slideIndex="4"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-05.jpg" alt="">
                                </div>                                
                            </a></li>
                            <li data-slideIndex="5"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-06.jpg" alt="">
                                </div>                                
                            </a></li>
                            <li data-slideIndex="0"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-01.jpg" alt="">
                                </div>                                
                            </a></li>
                            <li data-slideIndex="1"><a href="">
                                <div class="post-thumb"> 
                                    <img src="images/gallery/thumbs/img-02.jpg" alt="">
                                </div>                                
                            </a></li>
                         
                        
                        </ul>
                        <!-- gallery thumbs -->

                    </div>

                </div>
                <!-- Gallery Widget -->

                <!-- Slider Widget -->
                <div class="post-widget">

                    <!-- Heading -->
                    <div class="primary-heading">
                        <h2>most viewed posts</h2>
                    </div>
                    <!-- Heading -->

                    <!-- post slider -->
                    <div class="light-shadow white-bg p-30 slider-post"> 
                        <div id="post-slider-2">
                            
                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-01.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-02.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-03.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-04.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                           <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-01.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->


                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-02.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-03.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                           <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-04.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                           <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/post-1/img-01.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>

                    </div>
                    <!-- post slider -->

                </div>
                <!-- Slider Widget -->

                <!-- archieve widget -->
                <div class="post-widget">

                    <!-- Heading -->
                    <div class="primary-heading">
                        <h2>archieve posts</h2>
                    </div>
                    <!-- Heading -->

                    <!-- archieve -->
                    <div class="light-shadow white-bg"> 
                        <div class="archieve-tabs">
                        
                            <!-- Tabs Nav -->
                            <div class="cate-tab-navs">
                                <ul class="nav-justified">
                                    <li class="active">
                                        <a href="#january" data-toggle="tab">january</a>
                                    </li>
                                    <li>
                                        <a href="#february" data-toggle="tab">february</a>
                                    </li>
                                    <li>
                                        <a href="#march" data-toggle="tab">march</a>
                                    </li>
                                    <li>
                                        <a href="#april" data-toggle="tab">april</a>
                                    </li>
                                    <li>
                                        <a href="#may" data-toggle="tab">may</a>
                                    </li>
                                    <li>
                                        <a href="#june" data-toggle="tab">june</a>
                                    </li>
                                    <li>
                                        <a href="#july" data-toggle="tab">july</a>
                                    </li>
                                    <li>
                                        <a href="#august" data-toggle="tab">august</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- Tabs Nav -->

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="january">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/health/img-01.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="february">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/health/img-06.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                 <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="march">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/lifestyle/img-06.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                 <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="april">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/lifestyle/img-01.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                 <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="may">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/sports/img-06.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                 <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="june">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/sports/img-01.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="july">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/fashion/img-06.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="august">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6 col-xs-12 left-border">
                                            <div class="post">
                                                <div class="post-thumb">
                                                    <img src="images/world/img-06.jpg" alt="">
                                                    <span class="post-badge"><i class="fa fa-camera"></i>16</span>
                                                    <div class="thumb-over">
                                                        <p><a href="#">Lorem ipsum dolor sit amet, consec adipisicing elit, sed do eiusmod tempor</a></p>
                                                    </div>
                                                </div>

                                                 <ul class="post-meta">
                                                    <li><i class="icon-user"></i>jessica alex</li>
                                                    <li><i class="icon-clock"></i>10 Min ago</li>
                                                    <li><i class="fa fa-thumbs-o-up"></i>20</li>
                                                    <li><i class="icon-speech-bubble"></i>20</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 col-xs-12">
                                            <div class="archieve-list">
                                                 <ul>
                                                    <li><a href="#">James blake fires the officer who did.</a></li>
                                                    <li><a href="#">prison incident follows earlier bollow.</a></li>
                                                    <li><a href="#">common persons mistakes should  ignore.</a></li>
                                                    <li><a href="#">6.6 magnitude earth quake hits gulf.</a></li>
                                                    <li><a href="#">hurbagh loses his mind on sideline.</a></li>
                                                    <li><a href="#">hinted at murder before cellmate.</a></li>
                                                    <li><a href="#">sports is being latest trend in news.</a></li>
                                                </ul> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                            <!-- Tab panes -->

                        </div>
                    </div>
                    <!-- archieve -->

                </div>
                <!-- archieve widget -->

                <!-- add banner -->
                <div class="add-banner text-center m-0 p-0">
                    <img src="images/add-3.jpg" alt="">
                </div>
                <!-- add banner -->

            </div>
            <!-- Content -->

            <!-- Sidebar -->
            <div class="col-lg-3 col-md-3 col-xs-12 custom-re">
                <div class="row">
                <aside class="side-bar grid">
                    <!-- Auther profile -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                    
                        <div class="widget light-shadow">
                            <h3 class="secondry-heading">our authers</h3>
                            <div class="auther-widget">
                                <div id="auther-slider-thumb>
                                    <a data-slide-index="0" href="#"><img src="http://www.itopplus.com/assets/upload/201707121731219.jpg" alt=""></a>
                                    
                                </div>
                              
                            </div>
                        </div>
                    </div>
                    <!-- Auther profile -->

                    <!-- Social Networks -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <ul class="aside-social">
                                <li class="fb">
                                    <a href="#"><i class="fa fa-facebook"></i><span>followers</span><em>100</em></a>
                                </li>
                                <li class="pi">
                                    <a href="#"><i class="fa fa-pinterest-p"></i><span>followers</span><em>100</em></a>
                                </li>
                                <li class="tw">
                                    <a href="#"><i class="fa fa-twitter"></i><span>followers</span><em>100</em></a>
                                </li>
                                <li class="gmail">
                                    <a href="#"><i class="fa fa-google-plus"></i><span>followers</span><em>100</em></a>
                                </li>
                                 <li class="sky">
                                    <a href="#"><i class="fa fa-skype"></i><span>followers</span><em>100</em></a>
                                </li>
                                <li class="yt">
                                    <a href="#"><i class="fa fa-youtube"></i><span>followers</span><em>100</em></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Social Networks -->

                    <!-- twitter widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">twitter feed</h3>
                            <ul class="twitter-feed" id="twitter-slider">

                                <!-- tweet -->
                                <li>
                                    <div class="twitter-brand-name">
                                        <h6><i class="fa fa-twitter"></i>
                                        <a href="#">@Envato Studio</a>
                                        <i class="fa fa-retweet pull-right"></i>
                                        </h6>
                                    </div>
                                    <div class="brand-name">
                                        <p>Could @bodletech screen tery life to new have lengths ?</p>
                                        <span class="site-link"><a href="#">http://www.telegraph.co.uk/</a>6 hour ago</span>
                                    </div>
                                </li> 
                                <!-- tweet -->

                                <!-- tweet -->
                                <li>
                                    <div class="twitter-brand-name">
                                        <h6><i class="fa fa-twitter"></i>
                                        <a href="#">@Envato Studio</a>
                                        <i class="fa fa-retweet pull-right"></i>
                                        </h6>
                                    </div>
                                    <div class="brand-name">
                                        <p>Could @bodletech screen tery life to new have lengths ?</p>
                                        <span class="site-link"><a href="#">http://www.telegraph.co.uk/</a>6 hour ago</span>
                                    </div>
                                </li> 
                                <!-- tweet -->

                            </ul>
                        </div>
                    </div>
                    <!-- twitter widget -->

                    <!-- News Widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">top news</h3>
                            <div class="horizontal-tabs-widget">

                                <!-- tabs navs -->
                                <ul class="theme-tab-navs">
                                    <li class="active">
                                        <a href="#week" data-toggle="tab">week</a>
                                    </li>
                                    <li>
                                        <a href="#month" data-toggle="tab">month</a>
                                    </li>
                                    <li>
                                        <a href="#all" data-toggle="tab">all</a>
                                    </li>
                                </ul>
                                <!-- tabs navs -->
  
                                <!-- Tab panes -->
                                <div class="horizontal-tab-content tab-content">
                                    <div class="tab-pane fade active in" id="week">
                                        <ul class="post-wrap-list">
                                            <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-01.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                           <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-02.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors it..</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-03.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="month">
                                        <ul class="post-wrap-list">
                                            <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-01.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                           <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-02.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolor...</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-03.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane fade" id="all">
                                        <ul class="post-wrap-list">
                                            <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-01.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                           <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-02.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="post-wrap small-post">
                                                <div class="post-thumb">
                                                  <img src="images/sidebar/news/img-03.jpg" alt="post">
                                                </div>
                                                <div class="post-content">
                                                    <h4><a href="#">Lorem ipsum dolors...</a></h4>
                                                    <ul class="post-meta">
                                                        <li><i class="fa fa-clock-o"></i>25 dec, 2016</li>
                                                        <li><i class="fa fa-comments-o"></i>20</li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Tab panes -->

                            </div>
                        </div>
                    </div>
                    <!-- News Widget -->

                    <!-- Calender widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">Arcieve</h3>
                            <div class="calender-widget">
                                <div id="calendar" class="calendar"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Calender widget -->

                    <!-- Slider Widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">slider widget</h3>
                            <div class="slider-widget">

                                <!-- Slider -->
                                <div id="post-slider">
                                    <div class="item">
                                        <img src="images/sidebar/slider-01.jpg" alt="slider">
                                    </div>
                                    <div class="item">
                                        <img src="images/sidebar/slider-01.jpg" alt="slider">
                                    </div>
                                    <div class="item">
                                        <img src="images/sidebar/slider-01.jpg" alt="slider">
                                    </div>
                                </div>
                                <!-- Slider -->

                                <!-- Content -->
                                <div class="post-content">
                                    <h4><a href="#">Girl yoyo Music listing</a></h4>

                                    <!-- post meta -->
                                    <ul class="post-meta">
                                        <li><i class="icon-user"></i>jessica alex</li>
                                        <li><i class="icon-clock"></i>5 Min ago</li>
                                    </ul>
                                    <!-- post meta -->

                                    <p>Lorem ipsum dolor siting amet consec adipisicing elit, sed do eiusmod.</p>
                                </div>
                                <!-- Content -->

                            </div>
                        </div>
                    </div>
                    <!-- Slider Widget -->


                    <!-- facebook widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">find us on facebook</h3>
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=300&height=220&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" width="300" height="220" style="border:none;overflow:hidden"></iframe>
                        </div>
                    </div>
                    <!-- facebook widget -->

                    <!-- Catogires widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">categories</h3>
                            <ul class="categories-widget">
                                <li><a href="#"><em>fashion</em><span class="bg-green">15</span></a></li>
                                <li><a href="#"><em>world</em><span class="bg-masterd">15</span></a></li>
                                <li><a href="#"><em>technology</em><span class="bg-p-green">15</span></a></li>
                                <li><a href="#"><em>health</em><span class="bg-orange">15</span></a></li>
                                <li><a href="#"><em>lifestyle</em><span class="bg-gray">15</span></a></li>
                                <li><a href="#"><em>sports</em><span class="bg-masterd">15</span></a></li>
                              
                            </ul>
                        </div>
                    </div>
                    <!-- Catogires widget -->

                    <!-- sports Widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">sports news</h3>
                            <div class="horizontal-tabs-widget">

                                <!-- tabs navs -->
                                <ul class="theme-tab-navs style-2">
                                    <li class="active">
                                        <a href="#fixtures" data-toggle="tab">Last Match</a>
                                    </li>
                                    <li>
                                        <a href="#result" data-toggle="tab">upcomming</a>
                                    </li>
                                </ul>
                                <!-- tabs navs -->
  
                                <!-- Tab panes -->
                                <div class="horizontal-tab-content tab-content">
                                    <div class="tab-pane fade active in" id="fixtures">
                                        <div class="matches-detail">
                                            <p>49 Chapel Lane ARNE BH20 12/02/2016 / 19:00</p>
                                            <div class="team-btw-match">
                                                <ul>
                                                    <li>
                                                        <img src="images/team-logos/img-03.png" alt="">
                                                        <span>Munchester <br> united</span>
                                                    </li>
                                                    <li>
                                                        <img src="images/team-logos/img-04.png" alt="">
                                                        <span>Norwich <br> City</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="goals-detail">
                                                <ul>
                                                    <li>
                                                        <span>M. Johansen</span>
                                                        <span>23 (FNA)</span>
                                                    </li>
                                                    <li>
                                                        <span>G. Smith</span>
                                                        <span>41 (FNA)</span>
                                                    </li>
                                                    <li>
                                                        <span>T. Mosler</span>
                                                        <span>59 (FNA)</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="result">
                                        <div class="upcoming-fixture">
                                        <div class="table-responsive">
                                            <table class="table m-0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="logo-width-name"><img src="images/team-logos-small/img-01.png" alt="">South</div>
                                                        </td>
                                                        <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="logo-width-name"><img src="images/team-logos-small/img-03.png" alt="">South</div>
                                                        </td>
                                                        <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="logo-width-name"><img src="images/team-logos-small/img-05.png" alt="">South</div>
                                                        </td>
                                                        <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="logo-width-name"><img src="images/team-logos-small/img-07.png" alt="">South</div>
                                                        </td>
                                                        <td><span class="upcoming-fixture-date">6 Feb 2016 15:00</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <!-- Tab panes -->

                            </div>
                        </div>
                    </div>
                    <!-- sports Widget -->

                    <!-- advertisement widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">advertisement</h3>
                            <div class="add">
                                <a href="#"><img src="images/add.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <!-- advertisement widget -->

                    <!-- advertisement widget -->
                    <div class="grid-item col-lg-12 col-md-12 col-sm-4 col-xs-6 r-full-width">
                        <div class="widget">
                            <h3 class="secondry-heading">online pool</h3>
                            <div class="pool-widget">
                                <p>Type and scrambeld it to make a type specimen book:</p>
                                <form role="form">
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Twice a year</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Once a year</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">Only when i need it</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio">I'm healthy</label>
                                    </div>
                                </form>
                                <div class="group">
                                    <a href="#" class="btn red">vote now</a>
                                    <a href="#" class="btn red">view now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- advertisement widget -->
                </aside>
                </div>
            </div>
            <!-- Sidebar -->

        </div>   
        <!-- Main Content Row -->

    </div>
</div>  

<!-- categories posts -->
<div class="post-widget m-0 white-bg">
    <div class="cate-tab-navs">
        <div class="container">
            <ul class="nav-justified">
                <li class="active"><a data-target="#politics" data-toggle="tab">politics</a></li>
                <li><a data-target="#technology" data-toggle="tab">technology</a></li>
                <li><a data-target="#fashion" data-toggle="tab">fashion</a></li>
                <li><a data-target="#international" data-toggle="tab">international</a></li>
                <li><a data-target="#admin" data-toggle="tab">admin pick</a></li>
                <li><a data-target="#top" data-toggle="tab">top posts</a></li>
            </ul>
        </div>
    </div>
    <div class="cate-tab-content theme-padding">
        <div class="container">
            <div class="tab-content">
                <div class="tab-pane active fade in" id="politics">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/world/img-01.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/world/img-02.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/world/img-03.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/world/img-05.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="technology">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/technology/img-05.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/technology/img-02.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/technology/img-03.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/technology/img-04.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="fashion">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/fashion/img-05.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/fashion/img-04.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/fashion/img-03.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/fashion/img-08.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="international">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/world/img-08.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/world/img-07.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/fashion/img-06.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/world/img-05.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="admin">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/sports/img-08.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/sports/img-07.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/technology/img-08.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/lifestyle/img-08.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="top">
                    <div class="row slider-post">
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/technology/img-04.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->

                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/lifestyle/img-05.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/technology/img-02.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                        <div class="col-sm-3 col-xs-6">

                            <!-- post -->
                            <div class="post style-1">

                                <!-- post img -->
                                <div class="post-thumb"> 
                                    <img src="images/health/img-03.jpg" alt="">
                                    <span class="post-badge">post</span>
                                    <div class="thumb-hover">
                                        <div class="position-center-center">
                                            <a href="#" class="fa fa-link"></a>
                                        </div>
                                    </div>                                                
                                </div>
                                <!-- post img -->

                                <!-- post details -->
                                <div class="post-content">
                                    <h4><a href="post-detail.html">Full Responsive amazing design pixel perfect</a></h4>
                                    <ul class="post-meta m-0">
                                        <li><i class="icon-user"></i>jessica</li>
                                        <li><i class="icon-clock"></i>2 Min ago</li>
                                    </ul>
                                </div>
                                <!-- post details -->

                            </div>
                            <!-- post -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>     
<!-- categories posts -->

@stop