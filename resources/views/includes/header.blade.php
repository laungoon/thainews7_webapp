<!-- Header --> 
<div class="header-wrap">

    <!-- Top Bar -->
    <div class="top-bar">
        <div class="container">
            <div class="row">

                <!-- Top Left Nav -->
                <div class="col-sm-6 col-xs-6">
                    <ul class="top-left">
                        <li><i class="fa fa-phone"></i>092-2765384</li>     
                        <li><i class="fa fa-envelope-o"></i>info@thainews7.com</li>
                    </ul>
                </div>
                <!-- Top Left Nav -->

                <!-- Top Right Nav -->
                <div class="col-sm-6 col-xs-6 r-full-width">
                    <ul class="top-right text-right">
                        <!-- <li class="md-trigger" data-modal="login-popup"><a href="#"><i class="fa fa-key">
                        </i>login</a></li>
                        <li class="md-trigger" data-modal="register-popup"><a href="#"><i class="icon-user"></i>register</a></li> -->
                        
                        <!-- cart dropdown -->
                        <li class="cart dropdown">
                            <a href="#" class="dropdown-toggle btn-cart" data-toggle="dropdown">
                            <i class="fa fa-shopping-cart"></i><span>2</span></a>

                            <div class="dropdown-menu cart-item"> 
                                <h3>Recently added item(s)</h3>
                                <ul class="cart-list">
                                    <li>
                                        <div class="cart-product-item">
                                            <div class="product-img">
                                                <img src="images/cart-1.jpg" alt="image description">
                                            </div>
                                            <div class="detail">
                                                <span class="product-title">Pearl Izumi Men's E:Motion Tri N1 Neutral Race Shoe</span>
                                                <span class="price">1 x $124.99</span>
                                            </div>
                                            <a class="btn-delete-item">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="cart-product-item">
                                            <div class="product-img">
                                                <img src="images/cart-2.jpg" alt="image description">
                                            </div>
                                            <div class="detail">
                                                <span class="product-title">Pearl Izumi Men's E:Motion Tri N1 Neutral Race Shoe</span>
                                                <span class="price">1 x $124.99</span>
                                            </div>
                                            <a class="btn-delete-item">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="cart-product-item">
                                            <div class="product-img">
                                                <img src="images/cart-3.jpg" alt="image description">
                                            </div>
                                            <div class="detail">
                                                <span class="product-title">Pearl Izumi Men's E:Motion Tri N1 Neutral Race Shoe</span>
                                                <span class="price">1 x $124.99</span>
                                            </div>
                                            <a class="btn-delete-item">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="btn-area">
                                    <a href="#" class="btn btn-viewall pull-left red">View All</a>
                                    <a href="#" class="btn btn-checkout pull-right red">Checkout</a>
                                </div>
                            </div>

                        </li>
                        <!-- cart dropdown -->

                    </ul>
                </div>
                <!-- Top Right Nav -->

            </div>
        </div>
    </div>
    <!-- Top Bar -->


    <!-- Navigation Holder -->
    <header class="header">
        <div class="container">
            <div class="nav-holder">

                <!-- Logo Holder -->
                <div class="logo-holder">
                    <a href="#"><img src={{asset('/upload/news/l2.png')}} alt="THAINEWS7"></a>
                    THAINEWS 7
                </div>
                <!-- Logo Holder -->

                <!-- Navigation -->
                <div class="cr-navigation">

                    <!-- Navbar -->
                     <nav class="cr-nav">
                        <ul>
                            <li>
                                <a href="index.html">หน้าแรก</a>                              
                            </li>
                            <li>
                                <a href="#">ข่าวบันเทิง</a>
                                
                            </li>
                            <li>
                                <a href="#">เจาะข่าวเด่น</a>
                                
                            </li>
                           <li>
                                <a href="#">ท่องเที่ยว</a>
                            </li>
                            <li>
                                <a href="#">เจาะหลังไมค์</a>
                            </li>
                            <li>
                                <a href="#">ติดต่อเรา</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- Navbar -->

                    <ul class="cr-add-nav">
                        <li><a href="#" class="icon-search md-trigger" data-modal="search-popup"></a></li>
                        <li><a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a></li>
                    </ul>

                </div>
                <!-- Navigation -->

            </div>
        </div>
    </header>
    <!-- Navigation Holder -->

</div>
<!-- Header -->

<?php
if($module == "")
{
?>
<!-- Banner slider -->
<div class="banner-slider">
    
    <!-- slider main slides -->
    
     
    <div id="ninja-slider" class="ninja-slider">
         
        <div class="slider-inner"> 
            <ul>
             
                <li>
                    <a class="ns-img" href="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/21617991_1866027747046604_5982643899014127649_n.jpg?oh=7f254998cd9df90f553f39a34e432c42&oe=5A3B3EDE"></a>
                    <!-- caption -->
                    <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">News Top 10</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>บอสแอนผู้บริหารอาหารเสริม Saytaa เปิดตัว ฟรีเซนเตอร์ 3 สาว ลำใย ใหทองคำ,ธัญญ่า อาร์สยาม,อาม ชุติมา</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
       


                <li>
                    <a class="ns-img" href="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/21462986_786223728215219_8056793366540466067_n.jpg?oh=1b35692be9d4deed558072a5090ef72d&oe=5A5E36DE"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">ประทับใจสุดๆ ไปกับ “คอนเสิร์ต 30 ปี คำภีร์ แกเพื่อนฉัน”</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-0/p320x320/21463121_784973925006866_9064746919394561705_n.jpg?oh=35ab879f0a884c8d763c199043592bbe&oe=5A406FD0"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">เทศบาลเมืองเพชรบูรณ์ มอบกรวยจราจรและป้ายไฟสามเหลี่ยมหยุดตรวจชนิดโปร่ง ให้กับสถานีตำรวจภูธรเมืองเพชรบูรณ์</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>thainews7</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-0/p235x350/21751777_789103774593881_8937568946286857444_n.jpg?oh=ad1f2ff0e47549f9a32c04ff12853368&oe=5A4206DF"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">คลินิกแพทย์ผิวหนังและความงาม ได้มีการนำนวัตกรรมล้ำยุคในการลบรอยสัก การกำจัดขน และการรักษาแผลเป็น </a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>Thainews7</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>ลานอเนกประสงค์ ชั้น 1 ศูนย์การค้าเซ็นทรัลมารีนา พัทยา จ.ชลบุรี คลินิกแพทย์ผิวหนังและความงาม Dr.TATTOF(ดอกเตอร์แทตออฟ) ภายใต้การบริหารโดย นพ.นัทธพงศ์ จิรุระวงศ์ หรือหมอหนุ่ม </p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/21430590_1873932082924153_5690755851042214321_n.jpg?oh=fa73407382402fd0d34f777c9569c9b3&oe=5A4CB93E"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">ขอแสดงความยินดี กับ บก.เอกสิทธิ์ หมวดทอง นสพ.สื่อรัฐ และ ผู้อำนวยการสื่อรัฐทีวี </a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>Thainews7</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>บริษัท สื่อรัฐนิวส์ จำกัด ได้รับรางวัลสยามมโนรี จาก สภาศิลปวัฒนธรรมจิตตานุภาพ โดยมี เจ้าภาคินัย ณ เชียงใหม่</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/21430335_1873953556255339_644259286046448442_n.jpg?oh=4e770daa22bd2115e3eeb0c737bfbdd1&oe=5A88C1E5"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">Audi A5 Coupe 2017 ใหม่ นำเข้าทั้งคัน ราคา 3,299,000 บาท</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="https://s.isanook.com/au/0/ud/11/59901/151.jpg"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">LAudi A5 Coupe 2017 ใหม่ นำเข้าทั้งคัน ราคา 3,299,000 บาท</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="https://s.isanook.com/au/0/ud/11/59901/156.jpg"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">In the end, it's not the years in your life that count It's the life in your years</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->

                </li>
            </ul>
            <div class="fs-icon" title="Expand/Close"></div>
        </div>
    </div>
    <!-- slider main slides -->
    
    <!-- Banner Thumnail -->
    <div class="banner-thumbnail">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 pull-right">
                    <div id="thumbnail-slider">
                        <div class="inner">
                            <ul class="post-wrap-list">
                            

                                <li class="active">
                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/21617991_1866027747046604_5982643899014127649_n.jpg?oh=7f254998cd9df90f553f39a34e432c42&oe=5A3B3EDE" alt="post">
                                        </div>
                                        <div class="post-content">
                                         <h4><a href="#">News Top 10xxxxx</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>

                                <li>

                                    <div class="post-wrap small-post">

                                         <div class="post-thumb">
                                            <img src="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/21462986_786223728215219_8056793366540466067_n.jpg?oh=1b35692be9d4deed558072a5090ef72d&oe=5A5E36DE" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">บอสแอนผู้บริหารอาหารเสริม Saytaa เปิดตัว ฟรีเซนเตอร์ 3 สาว ลำใย ใหทองคำ,ธัญญ่า อาร์สยาม,อาม ชุติมา</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>
                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-0/p320x320/21463121_784973925006866_9064746919394561705_n.jpg?oh=35ab879f0a884c8d763c199043592bbe&oe=5A406FD0" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">เทศบาลเมืองเพชรบูรณ์ มอบกรวยจราจรและป้ายไฟสามเหลี่ยมหยุดตรวจชนิดโปร่ง ให้กับสถานีตำรวจภูธรเมืองเพชรบูรณ์”</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>
                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-0/p235x350/21751777_789103774593881_8937568946286857444_n.jpg?oh=ad1f2ff0e47549f9a32c04ff12853368&oe=5A4206DF" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">คลินิกแพทย์ผิวหนังและความงาม ได้มีการนำนวัตกรรมล้ำยุคในการลบรอยสัก การกำจัดขน และการรักษาแผลเป็น </a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>
                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/21430095_1873931492924212_2058795140880215054_n.jpg?oh=4a0a98a7f0a78b837a7462b82fcdd3a3&oe=5A569144" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">ขอแสดงความยินดี กับ บก.เอกสิทธิ์ หมวดทอง นสพ.สื่อรัฐ และ ผู้อำนวยการสื่อรัฐทีวี บริษัท สื่อรัฐนิวส์ จำกัด </a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>
                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="images/banner/post/img-02.jpg" alt="post">
                                        </div>
                                        <div class="post-content">
                                           <h4><a href="#">Confirm Information Thainews7</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>
                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="images/banner/post/img-02.jpg" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">Confirm Information Thainews7</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>
                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="images/banner/post/img-02.jpg" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">Confirm Information Thainews7</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>

                          

                            </ul>
                        </div>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
    <!-- Banner Thumnail -->

</div>
<!-- Banner slider -->
<?php
}
?>
