<!-- Header -->
<div class="header-wrap">

    <!-- Top Bar -->
    <div class="top-bar">
        <div class="container">
            <div class="row">

                <!-- Top Left Nav -->
                <div class="col-sm-6 col-xs-6">
                    <ul class="top-left">
                        <li><i class="fa fa-phone"></i>+84 868.8568</li>     
                        <li><i class="fa fa-envelope-o"></i>support@example.com</li>
                    </ul>
                </div>
                <!-- Top Left Nav -->

                <!-- Top Right Nav -->
                <div class="col-sm-6 col-xs-6 r-full-width">
                    <ul class="top-right text-right">
                        <li class="md-trigger" data-modal="login-popup"><a href="#"><i class="fa fa-key">
                        </i>login</a></li>
                        <li class="md-trigger" data-modal="register-popup"><a href="#"><i class="icon-user"></i>register</a></li>
                        
                        <!-- cart dropdown -->
                        <li class="cart dropdown">
                            <a href="#" class="dropdown-toggle btn-cart" data-toggle="dropdown">
                            <i class="fa fa-shopping-cart"></i><span>2</span></a>

                            <div class="dropdown-menu cart-item"> 
                                <h3>Recently added item(s)</h3>
                                <ul class="cart-list">
                                    <li>
                                        <div class="cart-product-item">
                                            <div class="product-img">
                                                <img src="{{ asset('theme/images/cart-1.jpg') }}" alt="image description">
                                            </div>
                                            <div class="detail">
                                                <span class="product-title">Pearl Izumi Men's E:Motion Tri N1 Neutral Race Shoe</span>
                                                <span class="price">1 x $124.99</span>
                                            </div>
                                            <a class="btn-delete-item">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="cart-product-item">
                                            <div class="product-img">
                                                <img src="{{ asset('theme/images/cart-2.jpg') }}" alt="image description">
                                            </div>
                                            <div class="detail">
                                                <span class="product-title">Pearl Izumi Men's E:Motion Tri N1 Neutral Race Shoe</span>
                                                <span class="price">1 x $124.99</span>
                                            </div>
                                            <a class="btn-delete-item">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="cart-product-item">
                                            <div class="product-img">
                                                <img src="{{ asset('theme/images/cart-3.jpg') }}" alt="image description">
                                            </div>
                                            <div class="detail">
                                                <span class="product-title">Pearl Izumi Men's E:Motion Tri N1 Neutral Race Shoe</span>
                                                <span class="price">1 x $124.99</span>
                                            </div>
                                            <a class="btn-delete-item">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="btn-area">
                                    <a href="#" class="btn btn-viewall pull-left red">View All</a>
                                    <a href="#" class="btn btn-checkout pull-right red">Checkout</a>
                                </div>
                            </div>

                        </li>
                        <!-- cart dropdown -->

                    </ul>
                </div>
                <!-- Top Right Nav -->

            </div>
        </div>
    </div>
    <!-- Top Bar -->

    <!-- Navigation Holder -->
    <header class="header">
        <div class="container">
            <div class="nav-holder">

                <!-- Logo Holder -->
                <div class="logo-holder">
                    <a href="index.html" class="inner-logo"></a>
                </div>
                <!-- Logo Holder -->

                <!-- Navigation -->
                <div class="cr-navigation">

                    <!-- Navbar -->
                     <nav class="cr-nav">
                        <ul>
                            <li>
                                <a href="index.html">home</a>
                                <ul>
                                   <li><a href="index-2.html">Home 2</a></li> 
                                   <li><a href="index-3.html">Home 3</a></li> 
                                </ul>
                            </li>
                            <li>
                                <a href="#">categories</a>
                                <ul>
                                   <li><a href="listing-page-1.html">fashion</a></li> 
                                   <li><a href="listing-page-2.html">sports</a></li> 
                                   <li><a href="listing-page-3.html">world</a></li> 
                                   <li><a href="listing-page-4.html">lifestyle</a></li> 
                                   <li><a href="listing-page-5.html">technology</a></li> 
                                   <li><a href="listing-page-6.html">health</a></li> 
                                   <li><a href="listing-page-7.html">MISC</a></li> 
                                </ul>
                            </li>
                            <li>
                                <a href="#">post</a>
                                <ul>
                                   <li><a href="video-post.html">video post</a></li> 
                                   <li><a href="slider-post.html">slider post</a></li> 
                                   <li><a href="post-detail.html">post detail</a></li> 
                                   <li><a href="post-detail-full-width.html">post detail full width</a></li> 
                                </ul>
                            </li>
                            <li>
                                <a href="#">shop</a>
                                <ul>
                                   <li><a href="products.html">products</a></li> 
                                   <li><a href="products-detail.html">products detail</a></li> 
                                </ul>
                            </li>
                            <li>
                                <a href="#">contact</a>
                                <ul>
                                   <li><a href="contact.html">contact 1</a></li> 
                                   <li><a href="contact-2.html">contact 2</a></li> 
                                   <li><a href="contact-3.html">contact 3</a></li> 
                                </ul>
                            </li>
                            <li>
                                <a href="#">404</a>
                                 <ul>
                                   <li><a href="error-404-1.html">404 style 1</a></li> 
                                   <li><a href="error-404-2.html">404 style 2</a></li> 
                                </ul>
                            </li>
                            <li>
                                <a href="#">pages</a>
                                <ul>
                                   <li><a href="faq.html">faq</a></li> 
                                   <li><a href="comming-soon.html">comming soon</a></li> 
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- Navbar -->

                    <ul class="cr-add-nav">
                        <li><a href="#" class="icon-search md-trigger" data-modal="search-popup"></a></li>
                        <li><a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a></li>
                    </ul>

                </div>
                <!-- Navigation -->

            </div>
        </div>
    </header>
    <!-- Navigation Holder -->

</div>
<!-- Header -->

<!-- Banner slider -->
<div class="banner-slider">
    
    <!-- slider main slides -->
    <div id="ninja-slider" class="ninja-slider">
        <div class="slider-inner">
            <ul>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-01.jpg') }}"></a>
                    <!-- caption -->
                    <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">Google is on a journey but we won't really know where it leads until fall</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-02.jpg') }}"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">I may not have gone where I intended to go, but I think I have ended up where I needed to be</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-03.jpg') }}"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">In the end, it's not the years in your life  that count It's the life in your years</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-04.jpg') }}"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">I may not have gone where I intended to go, but I think I have ended up where I needed to be</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-05.jpg') }}"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">Life is not a problem to be solved, but a reality to be experienced.</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-06.jpg') }}"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">Love is when the other person's happiness is more important than your own</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-07.jpg') }}"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">Life is not a problem to be solved, but a reality to be experienced.</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->
                </li>
                <li>
                    <a class="ns-img" href="{{ asset('theme/images/banner/img-08.jpg') }}"></a>
                    <!-- caption -->
                     <div class="caption-holder">
                        <div class="container p-relative">
                            <div class="caption">
                                <span>lifestyle</span>
                                <h2><a href="post-detail.html">In the end, it's not the years in your life that count It's the life in your years</a></h2>
                                <div class="post-meta">
                                    <span><i class="icon-user"></i>jessica alex</span>
                                    <span><i class="icon-clock"></i>10 Min ago</span>
                                    <span><i class="fa fa-thumbs-o-up"></i>20</span>
                                    <span><i class="icon-speech-bubble"></i>20</span>
                                </div>
                                <p>5 Luxury Travel Alternatives For The London Underground Strikes <br>For The London Underground Strikes</p>
                            </div>
                        </div>
                    </div>
                    <!-- caption -->

                </li>
            </ul>
            <div class="fs-icon" title="Expand/Close"></div>
        </div>
    </div>
    <!-- slider main slides -->
    
    <!-- Banner Thumnail -->
    <div class="banner-thumbnail">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 pull-right">
                    <div id="thumbnail-slider">
                        <div class="inner">
                            <ul class="post-wrap-list">

                                <li class="active">
                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('theme/images/banner/post/img-01.jpg') }}" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">Google is on a journey but we won't really know where it leads until fall</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>

                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('theme/images/banner/post/img-02.jpg') }}" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">I may not have gone whrere.</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                            
                                </li>

                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('theme/images/banner/post/img-03.jpg') }}" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">In the end its not years.</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>

                                </li>

                                <li>

                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('theme/images/banner/post/img-04.jpg') }}" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">I may not have gone where.</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>

                                </li>

                                <li>
                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('theme/images/banner/post/img-05.jpg') }}" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">Life is not a problem to be solved...</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>

                                    </div>
                                
                                </li>

                                <li>
                                    <div class="post-wrap small-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('theme/images/banner/post/img-06.jpg') }}" alt="post">
                                        </div>
                                        <div class="post-content">
                                            <h4><a href="#">Love is when other person's.</a></h4>
                                            <ul class="post-meta">
                                                <li><i class="icon-clock"></i>10 Min ago</li>
                                                <li><i class="icon-speech-bubble"></i>20</li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </div>
    <!-- Banner Thumnail -->

</div>
<!-- Banner slider -->

<!-- News Headline Container -->
<div class="news-bar white-bg">
    <div class="container">
        <div class="row">

            <!-- news -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-9 r-full-width">
                <div class="headline-wrap">

                    <span class="badge">just in</span>

                    <!-- news ticker -->
                    <div id="ticker">
                        <div class="clip">
                            <div class="today">
                                <ul>
                                    <li>Source: Manziel 'hung over' at Browns' meeting Clippers suspend Griffin four games.</li>
                                    <li> "Human &amp; Rights” has been helping the and surviving family Rights” know when  has been.</li>
                                    <li>Source: Manziel 'hung over' at Browns' meeting Clippers suspend Griffin four games.</li>
                                    <li>Source: Manziel 'hung over' at Browns' meeting Clippers suspend Griffin four games.</li>
                                </ul>
                            </div>
                          </div>
                    </div>
                    <!-- news ticker -->

                    <!-- ticker spinner -->
                    <div class="alert-spinner">
                        <div class="double-bounce1"></div>
                        <div class="double-bounce2"></div>
                    </div>
                    <!-- ticker spinner -->

                </div>
            </div>
            <!-- news -->

            <!-- time clock -->
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 r-full-width">  
                <div class="time-clock">                  
                    <div id="clock"></div>
                </div>
            </div>
            <!-- time clock -->

            <!-- Wheather forecast -->
            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                <div class="weather-holder">
                    <img src="{{ asset('theme/images/cloud.png') }}" alt="">
                    <span class="weather-state">Cloudy USA, Natha</span>
                    <span class="temp"><i class="fa fa-plus"></i> 18<sup>0</sup> C</span>
                </div>
            </div>
            <!-- Wheather forecast -->
            
        </div>
    </div>
</div>
<!-- News Headline Container -->