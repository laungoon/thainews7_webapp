<?php
  $dashboard = (str_contains(url()->current(), 'dashboard') == 1) ? "active" : "";
  $profile = (str_contains(url()->current(), 'profile') == 1) ? "active" : "";
  $friends = (str_contains(url()->current(), 'friends') == 1) ? "active" : "";
  $events = (str_contains(url()->current(), 'events') == 1) ? "active" : "";
  //print_r((boolean)$_SESS_SHOP['isApproved']);
?>

<ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">

    <li class="subheader">
        <span>APPS</span>
    </li>

    <li class="nav-item" role="tab" id="heading-dashboards">

        <a class="nav-link ripple with-arrow collapsed" <?php echo $dashboard; ?>
           data-toggle="collapse"
           data-target="#collapse-dashboards"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-dashboards">
            <i class="icon s-4 icon-tile-four"></i>
            <span>Dashboards</span>
        </a>

        <li class="nav-item">
            <a class="nav-link ripple <?php echo $profile; ?>" href="/profile"
               data-page-url="profile">
                <i class="icon s-4 icon-account"></i>
                <span>Profile</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link ripple <?php echo $events; ?>" href="/myevents"
               data-page-url="friend">
                <i class="icon s-4 icon-account"></i>
                <span>Event</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link ripple <?php echo $friends; ?>" href="/friends"
               data-page-url="friend">
                <i class="icon s-4 icon-account"></i>
                <span>Friend</span>
            </a>
        </li>

        <li class="nav-item" role="tab" id="heading-ecommerce">

          <a class="nav-link ripple with-arrow collapsed"
             data-toggle="collapse"
             data-target="#collapse-ecommerce"
             href="#"
             aria-expanded="false"
             aria-controls="collapse-ecommerce">
              <i class="icon s-4 icon-cart"></i>
              <span>Import</span>
          </a>

          <ul id="collapse-ecommerce"
              class="collapse"
              role="tabpanel"
              aria-labelledby="heading-ecommerce"
              data-children=".nav-item">

              <li class="nav-item">
                  <a class="nav-link ripple " href="/importExport"
                     data-page-url="/pages-profile.html">

                      <span>Friends</span>
                  </a>
              </li>

          </ul>
        </li>

        
    </li>

    <!-- 
    <li class="nav-item">
        <a class="nav-link ripple " href="apps-calendar.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-calendar-today"></i>
            <span>Calendar</span>
        </a>
    </li>

    <li class="nav-item" role="tab" id="heading-ecommerce">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-ecommerce"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-ecommerce">
            <i class="icon s-4 icon-cart"></i>
            <span>Ecommerce</span>
        </a>

        <ul id="collapse-ecommerce"
            class="collapse"
            role="tabpanel"
            aria-labelledby="heading-ecommerce"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple " href="apps-e-commerce-products.html"
                   data-page-url="/pages-profile.html">

                    <span>Products</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="apps-e-commerce-product.html"
                   data-page-url="/pages-profile.html">

                    <span>Product</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="apps-e-commerce-orders.html"
                   data-page-url="/pages-profile.html">

                    <span>Orders</span>
                </a>
            </li>

        </ul>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="apps-mail.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-email"></i>
            <span>Mail</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="apps-chat.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-hangouts"></i>
            <span>Chat</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="apps-file-manager.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-folder"></i>
            <span>File Manager</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="apps-contacts.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-account-box"></i>
            <span>Contacts</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="apps-todo.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-checkbox-marked"></i>
            <span>To-Do</span>
        </a>
    </li>

    <li class="subheader">
        <span>PAGES</span>
    </li>

    <li class="nav-item" role="tab" id="heading-authentication">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-authentication"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-authentication">
            <i class="icon s-4 icon-lock"></i>
            <span>Authentication</span>
        </a>

        <ul id="collapse-authentication"
            class="collapse"
            role="tabpanel"
            aria-labelledby="heading-authentication"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-auth-login.html"
                   data-page-url="/pages-profile.html">

                    <span>Login</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-auth-login-v2.html"
                   data-page-url="/pages-profile.html">

                    <span>Login v2</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-auth-register.html"
                   data-page-url="/pages-profile.html">

                    <span>Register</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-auth-register-v2.html"
                   data-page-url="/pages-profile.html">

                    <span>Register v2</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-auth-forgot-password.html"
                   data-page-url="/pages-profile.html">

                    <span>Forgot Password</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-auth-reset-password.html"
                   data-page-url="/pages-profile.html">

                    <span>Reset Password</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-auth-lock-screen.html"
                   data-page-url="/pages-profile.html">

                    <span>Lock Screen</span>
                </a>
            </li>

        </ul>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="pages-coming-soon.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-alarm-check"></i>
            <span>Coming Soon</span>
        </a>
    </li>

    <li class="nav-item" role="tab" id="heading-errors">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-errors"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-errors">
            <i class="icon s-4 icon-alert"></i>
            <span>Errors</span>
        </a>

        <ul id="collapse-errors"
            class="collapse"
            role="tabpanel"
            aria-labelledby="heading-errors"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-errors-404.html"
                   data-page-url="/pages-profile.html">

                    <span>404</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="pages-errors-500.html"
                   data-page-url="/pages-profile.html">

                    <span>500</span>
                </a>
            </li>

        </ul>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="pages-maintenance.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-oil"></i>
            <span>Maintenance</span>
        </a>
    </li>

    

    <li class="nav-item">
        <a class="nav-link ripple " href="pages-search.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-magnify"></i>
            <span>Search</span>
        </a>
    </li>

    <li class="subheader">
        <span>USER INTERFACE</span>
    </li>

    <li class="nav-item" role="tab" id="heading-elements">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-elements"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-elements">
            <i class="icon s-4 icon-layers"></i>
            <span>Elements</span>
        </a>

        <ul id="collapse-elements"
            class="collapse"
            role="tabpanel"
            aria-labelledby="heading-elements"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-alerts.html"
                   data-page-url="/pages-profile.html">

                    <span>Alerts</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-badges.html"
                   data-page-url="/pages-profile.html">

                    <span>Badges</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-breadcrumb.html"
                   data-page-url="/pages-profile.html">

                    <span>Breadcrumb</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-buttons.html"
                   data-page-url="/pages-profile.html">

                    <span>Buttons</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-button-group.html"
                   data-page-url="/pages-profile.html">

                    <span>Button Group</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-cards.html"
                   data-page-url="/pages-profile.html">

                    <span>Cards</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-dropdowns.html"
                   data-page-url="/pages-profile.html">

                    <span>Dropdowns</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-forms.html"
                   data-page-url="/pages-profile.html">

                    <span>Forms</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-input-group.html"
                   data-page-url="/pages-profile.html">

                    <span>Input Group</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-jumbotron.html"
                   data-page-url="/pages-profile.html">

                    <span>Jumbotron</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-list-group.html"
                   data-page-url="/pages-profile.html">

                    <span>List Group</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-navs.html"
                   data-page-url="/pages-profile.html">

                    <span>Navs</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-navbar.html"
                   data-page-url="/pages-profile.html">

                    <span>Navbar</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-pagination.html"
                   data-page-url="/pages-profile.html">

                    <span>Pagination</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-elements-progress.html"
                   data-page-url="/pages-profile.html">

                    <span>Progress</span>
                </a>
            </li>

        </ul>
    </li>

    <li class="nav-item" role="tab" id="heading-tables">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-tables"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-tables">
            <i class="icon s-4 icon-table-large"></i>
            <span>Tables</span>
        </a>

        <ul id="collapse-tables"
            class="collapse"
            role="tabpanel"
            aria-labelledby="heading-tables"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-tables-simple-table.html"
                   data-page-url="/pages-profile.html">

                    <span>Simple Table</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-tables-data-table.html"
                   data-page-url="/pages-profile.html">

                    <span>Data Table</span>
                </a>
            </li>

        </ul>
    </li>

    <li class="nav-item" role="tab" id="heading-page-layouts">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-page-layouts"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-page-layouts">
            <i class="icon s-4 icon-view-quilt"></i>
            <span>Page Layouts</span>
        </a>

        <ul id="collapse-page-layouts"
            class="collapse"
            role="tabpanel"
            aria-labelledby="heading-page-layouts"
            data-children=".nav-item">

            <li class="nav-item" role="tab" id="heading-carded">

                <a class="nav-link ripple with-arrow collapsed"
                   data-toggle="collapse"
                   data-target="#collapse-carded"
                   href="#"
                   aria-expanded="false"
                   aria-controls="collapse-carded">

                    <span>Carded</span>
                </a>

                <ul id="collapse-carded"
                    class="collapse"
                    role="tabpanel"
                    aria-labelledby="heading-carded"
                    data-children=".nav-item">

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-carded-full-width.html"
                           data-page-url="/pages-profile.html">

                            <span>Full Width</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-carded-left-sidebar.html"
                           data-page-url="/pages-profile.html">

                            <span>Left Sidebar</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-carded-right-sidebar.html"
                           data-page-url="/pages-profile.html">

                            <span>Right Sidebar</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item" role="tab" id="heading-simple">

                <a class="nav-link ripple with-arrow collapsed"
                   data-toggle="collapse"
                   data-target="#collapse-simple"
                   href="#"
                   aria-expanded="false"
                   aria-controls="collapse-simple">

                    <span>Simple</span>
                </a>

                <ul id="collapse-simple"
                    class="collapse"
                    role="tabpanel"
                    aria-labelledby="heading-simple"
                    data-children=".nav-item">

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-full-width.html"
                           data-page-url="/pages-profile.html">

                            <span>Full Width</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-left-sidebar.html"
                           data-page-url="/pages-profile.html">

                            <span>Left Sidebar</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-left-sidebar-inner.html"
                           data-page-url="/pages-profile.html">

                            <span>Left Sidebar Inner</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-left-sidebar-floating.html"
                           data-page-url="/pages-profile.html">

                            <span>Left Sidebar Floating</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-right-sidebar.html"
                           data-page-url="/pages-profile.html">

                            <span>Right Sidebar</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-right-sidebar-inner.html"
                           data-page-url="/pages-profile.html">

                            <span>Right Sidebar Inner</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-right-sidebar-floating.html"
                           data-page-url="/pages-profile.html">

                            <span>Right Sidebar Floating</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link ripple "
                           href="user-interface-page-layouts-simple-tabbed.html"
                           data-page-url="/pages-profile.html">

                            <span>Tabbed</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link ripple " href="user-interface-page-layouts-blank.html"
                   data-page-url="/pages-profile.html">

                    <span>Blank</span>
                </a>
            </li>

        </ul>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="user-interface-colors.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-palette"></i>
            <span>Colors</span>
        </a>
    </li>

    <li class="subheader">
        <span>COMPONENTS</span>
    </li>

    <li class="nav-item" role="tab" id="heading-charts">

        <a class="nav-link ripple with-arrow collapsed"
           data-toggle="collapse"
           data-target="#collapse-charts"
           href="#"
           aria-expanded="false"
           aria-controls="collapse-charts">
            <i class="icon s-4 icon-poll"></i>
            <span>Charts</span>
        </a>

        <ul id="collapse-charts"
            class="collapse"
            role="tabpanel"
            aria-labelledby="heading-charts"
            data-children=".nav-item">

            <li class="nav-item">
                <a class="nav-link ripple " href="components-charts-nvd3.html"
                   data-page-url="/pages-profile.html">

                    <span>nvD3</span>
                </a>
            </li>

        </ul>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="components-collapse.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-plus-box"></i>
            <span>Collapse</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="components-modal.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-window-maximize"></i>
            <span>Modal</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="components-popovers.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-tooltip-outline"></i>
            <span>Popovers</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="components-snackbar.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-page-layout-footer"></i>
            <span>Snackbar</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link ripple " href="components-tooltips.html"
           data-page-url="/pages-profile.html">
            <i class="icon s-4 icon-tooltip"></i>
            <span>Tooltips</span>
        </a>
    </li>
    -->

</ul>