
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content=""/>
<!-- Document Title -->
<title>THAINEWS 7</title>

<!-- StyleSheets -->
<link rel="stylesheet" href="{{ asset('theme/css/bootstrap/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/plugin.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/transition.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/icomoon.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/color-1.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('theme/css/font-awesome.min.css') }}">

<!-- Switcher CSS -->
<link href="{{ asset('theme/switcher/switcher.css') }}" rel="stylesheet" type="text/css"/> 
<link rel="stylesheet" id="skins" href="{{ asset('theme/css/color-1.css') }}" type="text/css" media="all">

<!-- FontsOnline -->
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Fira+Sans:400,300italic,300,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- JavaScripts -->
<script src="{{ asset('theme/js/modernizr.js') }}"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
