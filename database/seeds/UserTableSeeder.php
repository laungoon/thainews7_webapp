<?php
// app/database/seeds/UserTableSeeder.php

use Illuminate\Database\Seeder;
use App\User as User;

class UserTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('users')->delete();
	    User::create(array(
	        'name'     => 'Alekzander',
	        'email'    => 'laungoon@gmail.com',
	        'password' => Hash::make('secret'),
	    ));
	}

}