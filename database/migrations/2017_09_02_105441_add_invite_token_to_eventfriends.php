<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInviteTokenToEventfriends extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eventfriends', function (Blueprint $table) {
            //
            $table->string('invite_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventfriends', function (Blueprint $table) {
            //
            $table->dropColumn(
                'invite_token'
            );
        });
    }
}
