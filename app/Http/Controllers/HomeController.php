<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

use App\Models\News;

use Session;
use View;
use Auth;
use Image;
use DB;
use Hash;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsBannerSlider = DB::table('news')
                    ->where('category_id')
                    ->orderBy('created_at')
                    ->limit(10)
                    ->get();
        //ข่าวใหม่  update news
        $updateNews = DB::table('news')
                    ->where('category_id', '=', 1)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // เจาะข่าวเด่น ประเด็นเด็ด
        $newsCat1 = DB::table('news')
                    ->where('category_id', '=', 1)
                    ->orderBy('created_at', 'desc')
                    ->limit(1)
                    ->get();

        // บันเทิง Thainews7
        $newsCat2 = DB::table('news')
                    ->where('category_id', '=', 2)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // ศิลปะ วัฒธรรม ความเชื่อ
        $newsCat3 = DB::table('news')
                    ->where('category_id', '=', 3)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // ท่องเที่ยว แฟชั่น ความงาม
        $newsCat4 = DB::table('news')
                    ->where('category_id', '=', 4)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // เจาะหลังไมค์ ไทยลูกทุ่ง
        $newsCat5 = DB::table('news')
                    ->where('category_id', '=', 5)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // ข่าวประชาสัมพันธ์
        $newsCat6 = DB::table('news')
                    ->where('category_id', '=', 6)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // ก้าวทันโลก
        $newsCat7 = DB::table('news')
                    ->where('category_id', '=', 7)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // ข่าวเด่นภูมิภาค 77 จังหวัด
        $newsCat8 = DB::table('news')
                    ->where('category_id', '=', 8)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

        // Gallery Photos
        $newsCat9 = DB::table('news')
                    ->where('category_id', '=', 9)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();

       $slieder = DB::table('news') //สุขภาพ
               ->where('category_id', '=', 9)
                    ->orderBy('created_at', 'desc')
                    ->limit(5)
                    ->get();


        $data = ['newsCat1'     => $newsCat1, 
                 'newsCat2'     => $newsCat2, 
                 'newsCat3'     => $newsCat3, 
                 'newsCat4'     => $newsCat4, 
                 'newsCat5'     => $newsCat5,
                 'newsCat6'     => $newsCat6,
                 'newsCat7'     => $newsCat7,
                 'newsCat8'     => $newsCat8,
                 'newsCat9'     => $newsCat9,
                 'updateNews'   => $updateNews,
                 'slieders'     => $slieder,
                 'BannerSliders' => $newsBannerSlider];
                


    
        // print '<pre>';
        // print_r( $data );
        // print '</pre>';

        return view('pages.home.index')
               ->with('data', $data);

    }

    /**
     * Display the specified resource.s
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);
        // show the view and pass the friend to it
        return view('pages.news.index')->with('news', $news);

    }
}
