<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\News;

use Auth;
use Hash;
use View;
use Image;
use Session;

use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::get();

        // show the view and pass the friend to it
        return View::make('pages.admin.news.index')
            ->with('news', $news);
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cate = DB::table('category')
        ->get();
        // show the view and pass the friend to it
        return View::make('pages.admin.news.create')
        ->with('cate', $cate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $rules = array(
            'title'       => 'required',
            'content'     => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/news/create')
                ->withErrors($validator);
        } else {
            // store
            try 
            {
                $photoName = "";
                $file = $request->file('image');

                if($file)
                {
                    
                    // get current time and append the upload file extension to it,
                    // then put that name to $photoName variable.
                    $photoName = time().'.'.$file->getClientOriginalExtension();

                    /*
                    talk the select file and move it public directory and make avatars
                    folder if doesn't exsit then give it that unique name.
                    */
                    //$file->move(public_path('news\uploads\picture'), $photoName);
                    $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

                    $file->move($storagePath . '/news/uploads/', $photoName);
                }

                $news = new News;
                $news->category_id      = Input::get('category_id');
                $news->title            = Input::get('title');
                $news->content          = Input::get('content');
                $news->read             = 0;
                if($photoName) 
                    $news->image        = $photoName;
                $news->save();

                // redirect
                Session::flash('message', 'Successfully created news!');
                return Redirect::to('admin/news/');
            } 
            catch (Illuminate\Filesystem\FileNotFoundException $e) 
            {

            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $news = News::find($id);
        $cate = DB::table('category')
        ->get();
        // show the view and pass the friend to it
        return View::make('pages.admin.news.edit')
            ->with('news', $news)
            ->with('cate', $cate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // validate
        $rules = array(
            'title'       => 'required',
            'content'     => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/news/edit/'.$id)
                ->withErrors($validator);
        } else {
            // store
            try 
            {
                $photoName = "";
                $file = $request->file('image');

                if($file)
                {
                    
                    // get current time and append the upload file extension to it,
                    // then put that name to $photoName variable.
                    $photoName = time().'.'.$file->getClientOriginalExtension();

                    /*
                    talk the select file and move it public directory and make avatars
                    folder if doesn't exsit then give it that unique name.
                    */
                    //$file->move(public_path('news\uploads\picture'), $photoName);
                    $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

                    $file->move($storagePath . '/news/uploads/', $photoName);
                }

                $news = News::where('id', '=', $id)
                            ->update(['category_id' => Input::get('category_id'), 
                                      'title'       => Input::get('title'),
                                      'content'     => Input::get('content')]);


                if($photoName)
                {
                    $news = News::where('id', '=', $id)
                            ->update(['image' => $photoName]);
                }

                // redirect
                Session::flash('message', 'Successfully created news!');
                return Redirect::to('admin/news/');
            } 
            catch (Illuminate\Filesystem\FileNotFoundException $e) 
            {
                Session::flash('message', $e);
                return Redirect::to('admin/news/');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);

        $news->delete();
        return Redirect::to('admin/news');
    }
}
