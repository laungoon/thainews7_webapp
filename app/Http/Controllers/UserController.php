<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;

use Auth;
use Hash;
use App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Facebook;

class UserController extends Controller
{
    public function __construct( )
    {

    }

    public function index()
    {
        if (Auth::check())
            return view('pages.profile');
        else
            return view('pages.home');
    }

    public function showLogin()
    {
        return view('pages.signin');
    }

    public function authen( )
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:6' // password can only be alphanumeric and has to be greater than 6 characters
        );

        $validator = Validator::make( ['email' => Input::get('email'), 'password' => Input::get('password')] , $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails())
        {
            $messages = $validator->messages();
            return $messages;
            return Redirect::to('signin')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else
        {
            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata, true))
            {

                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                //return Auth::user();
                return Redirect::to('profile');

            }
            else
            {

                return Redirect::to('signin')->with(
                                                            array(
                                                                'LoginError' => 'Email or password invalid.'
                                                            )
                                                        );

            }

        }

    }

    public function Authlogout()
    {
        Auth::logout(); // logout user
        return Redirect::to('admin/signin');
    }

    public function getRegister()
    {
        return view('pages.admin.signup');
    }



}
