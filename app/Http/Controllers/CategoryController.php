<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Input;
use Redirect;
class CategoryController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
     public function __construct()
     {
         //$this->middleware('auth');
     }

    public function index()
    {
        $category = DB::table('category')
        //->where('id','=',1)
        ->get();
       
        // print_r($category);
        return view('pages.admin.category.index');
    }
}